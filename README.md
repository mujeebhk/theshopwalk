# ShopWalk

##run npm install to install the dependencies
 npm install

##run ng build to generate dist folder, dist folder is required to run express along the angular
ng build

##install nodemon using npm for development purpose
npm install -g nodemon


##once done run following command to start the server 
nodemon ./server.js

##server is running on port 3000 and all the configuration could be found under server.js inside the root of the project

