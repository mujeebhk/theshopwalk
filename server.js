var express = require('express'),
    errorHandler = require('express-error-handler'),
    http = require('http'),
    server;
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var winston = require('winston'); // Logger file provider
var proxy = require('express-http-proxy');
var middleware = require('http-proxy-middleware');
var compression = require('compression');
var cors = require('cors');
var app = express();
var sessionstore = require('sessionstore');
var db = require('./dbConfig')


//Production server config
var options = {
    target: 'http://localhost:9000', // target host
    changeOrigin: true, // needed for virtual hosted sites
    ws: true, // proxy websockets
    pathRewrite: {
        '^/app': '/app' // rewrite path
    },
    router: {
        // when request.headers.host == 'dev.localhost:3000',
        // override target 'http://www.example.org' to 'http://localhost:8000'
        'dev.localhost:3000': 'http://localhost:8000'
    }
};

var exampleProxy = middleware(options);
app.use('/app', exampleProxy);



// Configure the logging to a file.
var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.File)({
            name: 'info-file',
            filename: 'shopWalk-info.log',
            level: 'info'
        }),
        new(winston.transports.File)({
            name: 'debug-file',
            filename: 'shopWalk-debug.log',
            level: 'debug'
        }),
        new(winston.transports.File)({
            name: 'error-file',
            filename: 'shopWalk-error.log',
            level: 'error'
        })
    ]
});
// To stop logs in any of the logging files(Remove comment line)
// logger.remove('info-file');
//logger.remove('error-file');
//logger.remove('debug-file');

// view engine setup
app.disable('etag');
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression());
app.use(cors({credentials: true, origin: true}));
app.use(bodyParser.json({
    limit: "50mb"
}));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + '/dist'));
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));


//Session Filters
var adminSessionFilter = require('./server/sessionFilter/AdminSessionFilter');
adminSessionFilter.sessionFilter(app);

//REST Api Paths
var testApi = require('./server/restApis/test');
var admin = require('./server/restApis/Admin');
var retailer = require('./server/restApis/Retailer');
var city = require('./server/restApis/City');
var country = require('./server/restApis/Country');
var ads = require('./server/restApis/Ads');
var store = require('./server/restApis/Store');
var geoCoder = require('./server/restApis/GeoCoder');
var logs = require('./server/config/logsHandler')(logger);

app.use('/test', testApi);
app.use('/shopWalk/api/admin', admin);
app.use('/shopWalk/api/retailer', retailer);
app.use('/shopWalk/api/city', city);
app.use('/shopWalk/api/country', country);
app.use('/shopWalk/api/store', store);
app.use('/shopWalk/api/ads', ads);
app.use('/shopWalk/api/geoCoder', geoCoder);

//Set Port
const port = process.env.PORT || '9080';
app.set('port', port);
const shopWalkserver = http.createServer(app);
shopWalkserver.listen(port, () => console.log(`Running on localhost:${port}`));




// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// Send all other requests to the Angular app


function exitHandler(options, err) {
    if (options.cleanup) console.log('clean');
    if (err) console.log(err.stack);
    if (options.exit) process.exit();
}


//do something when app is closing
process.on('exit', exitHandler.bind(null, {
    cleanup: true
}));

//catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {
    exit: true
}));

//catches uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {
    exit: false
}));

module.exports = app;