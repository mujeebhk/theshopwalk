const Storage = require('@google-cloud/storage');
const Datastore = require('@google-cloud/datastore');
var ApplicationConstants = require('./server/common/ApplicationConstants');


exports.GoogleNoSql = new Datastore({
  projectId: ApplicationConstants.ProjectId,
  keyFilename: ApplicationConstants.KeyFilename,
});