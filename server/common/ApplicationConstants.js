module.exports = {

    //Session Details
    AVOID_AUTHENTICATED_SESSION_CHECK: false,
    secretKeyNameForSession: 'ShopWalkAdminDost1234', //secret key Name for Admin token
    shopWalkSessionTokenName: "shopWalk_access_token", //token Name used in cookies
    adminId: "adminId",
    tokenBlackListed: "blackListed",
    sessionExpireTime: 60 * 60 * 24 * 30, //sessionExpireTime set to one month
    enableSecureToken: false, //change to true while deploying to production

    //Cloudinary 
    Cloudinary_Cloud_Name: 'shab9686',
    Cloudinary_Api_Key: '281875635169296',
    Cloudinary_Api_Secret: '1jEPl3KA8EfJvz0IooXJiYV4iOU',

    //Google NoSql
    ProjectId: 'theshopwalk-211808',
    KeyFilename: './server/config/TheShopWalk-98991d623e85.json',
    AdminTable: 'Admin',
    RetailerTable: 'Retailer',
    StoreTable: 'Store',
    AdsTable: 'Ads',
    CityTable: 'City',
    CountryTable: 'Country',

    //Google GeoCoder
    GogoleGeoCodeUrl: "https://maps.googleapis.com/maps/api/geocode/json?address=",
    GogoleReverseGeoCodeUrl: "https://maps.googleapis.com/maps/api/geocode/json?latlng=",
    ApiKey: "&key=AIzaSyACd0ZFudkNX-1Ne7l9_L8YoC3CSLtifMM",

    //Common
    RetailerMainBranch: 0,

    //MongoDb
    ShopWalkDbUrl: 'mongodb://localhost:27017/shopWalk',
    RetailerCollection: 'Retailer',
    AdsCollection: 'Ads',

    DistanceinMiles: 1000,
    MinAdsToBeLoaded: 10
}