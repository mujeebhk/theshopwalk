var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
var entity = require('./Entity');
var async = require("async");
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');


//Create Retailer
router.post('/create', function (req, res, next) {

    logs.createLogs("Calling createStore() method");
    createStore(req, res, function (dbReturnValue) {
        if (res.statusCode == 500) {

            res.status(500);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});


//Create Store Db Process
function createStore(req, res, callback) {
    logs.createLogs("Entering createStore() method");

    req.body.creationDate = new Date();
    req.body.modificationDate = new Date();

    const taskKey = db.GoogleNoSql.key(ApplicationConstants.StoreTable);
    const storeEntity = entity.StoreEntity(req, taskKey);
    db.GoogleNoSql
        .save(storeEntity)
        .then(() => {
            logs.createLogs(`Store ${taskKey.id} created successfully.`);
            callback({
                id: parseInt(taskKey.id)
            });
        })
        .catch(err => {
            logs.createLogs('Error occured while creating Store. ERROR:' + err.stack);
            res.status(500);
            callback('ERROR:' + err.stack);
        });
}

//Update Retailer
router.post('/update', function (req, res, next) {

    logs.createLogs("Calling updateStore() method");
    updateStore(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Update Store Db Process
function updateStore(req, res, callback) {
    logs.createLogs("Entering updateStore() method");

    //checkForBadRequest
    if (req.body.id.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return callback('Bad Request.The id must be a string of 16 characters');
    }
    const transaction = db.GoogleNoSql.transaction();
    const taskKey = db.GoogleNoSql.key([ApplicationConstants.StoreTable, req.body.id]);

    transaction
        .run()
        .then(() => Promise.all([transaction.get(taskKey)]))
        .then(results => {
            const response = results[0];

            if (response[0] != null || response[0] != undefined) {

                //Configure Update Request
                // req.body.phone = response[0].phone; phone number will be passed as part of req obj
                req.body.creationDate = response[0].creationDate;
                req.body.modificationDate = new Date();

                const storeEntity = entity.StoreEntity(req, taskKey);
                transaction.save({
                    key: taskKey,
                    data: storeEntity.data,
                });
            } else {
                res.status(400);
                callback('Invalid Store Id');
            }
            return transaction.commit();
        })
        .then(() => {
            // The transaction completed successfully.
            if (res.statusCode != 400) {
                console.log(`Store ${req.body.id} updated successfully.`);
                callback(`Store ${req.body.id} updated successfully.`);
            }
        })
        .catch(err => {
            transaction.rollback()
            logs.createLogs('Error occured while updating Store. ERROR:' + err.details);
            res.status(500);
            callback('ERROR:' + err.details);
        });

}

//Fetch Store by Id
router.get('/getStore/:storeId', function (req, res, next) {

    logs.createLogs("Calling getStoreById() method to get retailer");
    getStoreById(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch Store Db Process
function getStoreById(req, res, callback) {
    logs.createLogs("Entering getStoreById() method");

    if (req.params.storeId.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return callback('Bad Request.The id must be a string of 16 characters');
    }

    const taskKey = db.GoogleNoSql.key(ApplicationConstants.StoreTable);
    taskKey.id = req.params.storeId;
    db.GoogleNoSql.get(taskKey).then(results => {

        if (results[0] != null) {
            logs.createLogs('Store fetched Suceessfully');
            const response = results[0];
            const taskKey = response[db.GoogleNoSql.KEY];
            response.id = parseInt(taskKey.id);
            callback(response);
        } else {
            logs.createLogs('Invalid Store Id. Id= ' + req.params.storeId);
            res.status(404);
            callback('Invalid Store Id');
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Store. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}


//Fetch Store List by retailerId
router.get('/getStoreList/:retailerId', function (req, res, next) {

    logs.createLogs("Calling getStoreList() method to get retailer");
    getStoreList(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch StoreList Db Process
function getStoreList(req, res, callback) {
    logs.createLogs("Entering getStoreList() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.StoreTable).filter('retailerId', '=', parseInt(req.params.retailerId)).order('retailerId');
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            const response = results[0];
            response.forEach(store => {
                const taskKey = store[db.GoogleNoSql.KEY];
                store.id = parseInt(taskKey.id);
            });
            logs.createLogs("Stroe List fetched Succesfully");
            callback(response);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Store list. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch Complete Stores count
router.get('/Count', function (req, res, next) {

    logs.createLogs("Calling getStoreCount() method");
    getStoreCount(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

function getStoreCount(req, res, callback) {
    logs.createLogs("Entering getStoreCount() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.StoreTable);
    db.GoogleNoSql.runQuery(query).then(results => {
        if (results[0] != null) {
            logs.createLogs('getStoreCount : Store List fetched Suceessfully');
            const response = results[0].length;
            callback(response);
        }
    }).catch(err => {
        logs.createLogs('Error occured while fetching Store count. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

module.exports = router;