var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
var entity = require('./Entity');
var async = require("async");
var cloudinary = require('../cloudinary/cloudinary');
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');
var request = require('request');

//Create Ad
router.post('/create', function (req, res, next) {

    logs.createLogs("Calling createAd() method");
    createAd(req, res, function (dbReturnValue) {
        if (res.statusCode == 500) {

            res.status(500);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Create Ad Db Process
function createAd(req, res, callback) {
    logs.createLogs("Entering createAd() method");

    req.body.creationDate = new Date();
    req.body.modificationDate = new Date();
    req.body.endDate = new Date(req.body.endDate);
    req.body.startDate = new Date(req.body.startDate);

    async.doWhilst(function (cb) {
        if (req.body.logopath != null) {

            cloudinary.adPdfOrImageUpload(req, res, function (cloudinaryRes) {
                if (cloudinaryRes[0] != null) {
                    if (cloudinaryRes.length != 1) {
                        req.body.adImageUrls = cloudinaryRes;
                        req.body.adPdf = [];
                    } else {
                        req.body.adImageUrls = [];
                        req.body.adPdf = cloudinaryRes[0];
                    }
                    delete req.body.logopath;
                    cb();
                } else {
                    return callback(cloudinaryRes);
                }
            })
        } else {
            req.body.adImageUrls = [];
            req.body.adPdf = [];
            cb();
        }
    }, function (cb) {

        const taskKey = db.GoogleNoSql.key(ApplicationConstants.AdsTable);
        const adsEntity = entity.AdsEntity(req, taskKey);
        db.GoogleNoSql
            .save(adsEntity)
            .then(() => {
                logs.createLogs(`Ad with id= ${taskKey.id} created successfully.`);
                callback({
                    id: parseInt(taskKey.id)
                });
            })
            .catch(err => {
                logs.createLogs('Error occured while creating Ad ERROR:' + err.stack);
                res.status(500);
                callback('ERROR:' + err.stack);
            });
    });
}

//Update Ads
router.post('/update', function (req, res, next) {

    logs.createLogs("Calling updateAds() method");
    updateAds(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Update Ads Db Process
function updateAds(req, res, callback) {
    logs.createLogs("Entering updateAds() method");

    //checkForBadRequest
    if (req.body.id.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return callback('Bad Request.The id must be a string of 16 characters');
    }

    req.body.modificationDate = new Date();
    async.doWhilst(function (cb) {
        if (req.body.logopath != null) {

            cloudinary.adPdfOrImageUpload(req, res, function (cloudinaryRes) {
                if (cloudinaryRes[0] != null) {
                    if (cloudinaryRes.length != 1) {
                        req.body.adImageUrls = cloudinaryRes;
                        req.body.adPdf = [];
                    } else {
                        req.body.adImageUrls = [];
                        req.body.adPdf = cloudinaryRes[0];
                    }
                    cb();
                } else {
                    return callback(cloudinaryRes);
                }
            })
        } else {
            cb();
        }
    }, function (cb) {

        const transaction = db.GoogleNoSql.transaction();
        const taskKey = db.GoogleNoSql.key([ApplicationConstants.AdsTable, req.body.id]);

        transaction
            .run()
            .then(() => Promise.all([transaction.get(taskKey)]))
            .then(results => {
                const response = results[0];

                if (response[0] != null || response[0] != undefined) {

                    //Configure Update Request
                    req.body.creationDate = response[0].creationDate;
                    req.body.retailerId = response[0].retailerId;
                    req.body.storeId = response[0].storeId;
                    req.body.modificationDate = new Date();
                    req.body.endDate = new Date(req.body.endDate);
                    req.body.startDate = new Date(req.body.startDate);
                    if (req.body.logopath == null) {
                        req.body.adImageUrls = response[0].adImageUrls ? response[0].adImageUrls : req.body.adImageUrls;
                        req.body.adPdf = response[0].adPdf ? response[0].adPdf : req.body.adPdf;
                    }

                    const adsEntity = entity.AdsEntity(req, taskKey);
                    transaction.save({
                        key: taskKey,
                        data: adsEntity.data,
                    });

                    return transaction.commit();
                } else {
                    res.status(400);
                    return callback('Invalid Ads Id');
                }
            })
            .then(() => {
                // The transaction completed successfully.

                if (res.statusCode != 400) {
                    console.log(`Ads ${req.body.id} updated successfully.`);
                    callback(`Ads ${req.body.id} updated successfully.`);
                }
            })
            .catch(err => {
                transaction.rollback()
                logs.createLogs('Error occured while updating Ads. ERROR:' + err.details);
                res.status(500);
                callback('ERROR:' + err.details);
            });
    });
}

//Fetch Ads by Id
router.get('/getAd/:adId', function (req, res, next) {

    logs.createLogs("Calling getAdsById() method");
    getAdsById(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch Ads Db Process
function getAdsById(req, res, callback) {
    logs.createLogs("Entering getAdsById() method");

    if (req.params.adId.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return callback('Bad Request.The id must be a string of 16 characters');
    }

    const taskKey = db.GoogleNoSql.key(ApplicationConstants.AdsTable);
    taskKey.id = req.params.adId;
    db.GoogleNoSql.get(taskKey).then(results => {

        if (results[0] != null) {
            logs.createLogs('Ad fetched Suceessfully');
            const response = results[0];
            const taskKey = response[db.GoogleNoSql.KEY];
            response.id = parseInt(taskKey.id);
            callback(response);
        } else {
            logs.createLogs('Invalid Ad Id. Id= ' + req.params.adId);
            res.status(404);
            callback('Invalid Ad Id');
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Ad. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch AdsList by retailerId
router.get('/getRetailerAdList/:retailerId', function (req, res, next) {

    logs.createLogs("Calling getAdsList() method");

    //checkForBadRequest
    if (req.params.retailerId.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return res.json('Bad Request.The id must be a string of 16 characters');
    }

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable)
        .filter('retailerId', '=', parseInt(req.params.retailerId));
    getAdsList(req, res, query, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch AdsList by storeId
router.get('/getStoreAdList/:storeId', function (req, res, next) {

    logs.createLogs("Calling getAdsList() method");

    //checkForBadRequest
    if (req.params.storeId.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return res.json('Bad Request.The id must be a string of 16 characters');
    }

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable)
        .filter('storeId', '=', parseInt(req.params.storeId));
    getAdsList(req, res, query, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch Ads List Db Process
function getAdsList(req, res, query, callback) {
    logs.createLogs("Entering getAdsList() method");

    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            logs.createLogs('Ads List fetched Suceessfully');
            const response = results[0];
            response.forEach(ad => {
                const taskKey = ad[db.GoogleNoSql.KEY];
                ad.id = parseInt(taskKey.id);
            });
            callback(response);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Ads List. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch AdsList by location
router.post('/getAdsListByLocation', function (req, res, next) {

    async.doWhilst(function (cb) {

        request(ApplicationConstants.GogoleGeoCodeUrl + req.body.address + ApplicationConstants.ApiKey, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                logs.createLogs("Addrees Co-Ordinates fethed successfully.The Lat Long = " + result.results[0].geometry.location.lat + "," + result.results[0].geometry.location.lng);
                req.params.lat = result.results[0].geometry.location.lat;
                req.params.long = result.results[0].geometry.location.lng;
                cb();
            } else {
                logs.createLogs("Addrees Co-Ordinates failed to fetch");
                res.status(500);
                return res.json('Error Occured while fetching Addrress Co-ordinates');
            }
        });
    }, function (end) {

        logs.createLogs("Calling getAdsListByLocation() method");
        getAdsListByLocation(req, res, function (dbReturnValue) {
            if (res.statusCode == 500 || res.statusCode == 400) {

                res.status(res.statusCode);
                return res.json(dbReturnValue);

            }
            res.status(200);
            res.json(dbReturnValue);


        });
    })

});

//Fetch Ads By Location Db Process
function getAdsListByLocation(req, res, callback) {
    logs.createLogs("Entering getAdsListByLocation() method");

    var adsResponseList = [];
    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable).filter('endDate', '>', new Date()).order('endDate');
    if (ApplicationConstants.MinAdsToBeLoaded > 0) {
        logs.createLogs("Entering getAdsListByLocation() method - MinAdsToBeLoaded :" + ApplicationConstants.MinAdsToBeLoaded);
        query.limit(ApplicationConstants.MinAdsToBeLoaded);
    }
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            logs.createLogs('getAdsListByLocation : Ads List fetched Suceessfully');
            const response = results[0];

            //Creating the AdsList by User Location
            async.doWhilst(function (cb) {
                response.forEach(ad => {

                    const taskKey = ad[db.GoogleNoSql.KEY];
                    ad.id = parseInt(taskKey.id);
                    if (ad && ad.storeDetails && ad.storeDetails.location != null && ad.startDate < new Date()) {
                        var distInMiles = getDistanceFromLatLonInMiles(ad.storeDetails.location.latitude, ad.storeDetails.location.longitude, req.params.lat, req.params.long);
                        logs.createLogs("getAdsListByLocation : ad.Id : " + ad.id);
                        logs.createLogs("getAdsListByLocation : distInMiles : " + distInMiles);
                        logs.createLogs("getAdsListByLocation : ApplicationConstants.DistanceinMiles : " + ApplicationConstants.DistanceinMiles);
                        if (distInMiles <= ApplicationConstants.DistanceinMiles) {
                            ad.distance = distInMiles;
                            logs.createLogs("getAdsListByLocation : added ad.Id : " + ad.id);
                            adsResponseList.push(ad);
                        }
                    }
                });
                cb();
            }, function (end) {
                callback(adsResponseList);
            })
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Ads List. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch Complete AdsList
router.get('/getAdsList', function (req, res, next) {

    logs.createLogs("Calling getCompleteAdsList() method");
    getCompleteAdsList(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch AdsList Db Process
function getCompleteAdsList(req, res, callback) {
    logs.createLogs("Entering getCompleteAdsList() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable).order('startDate', {
        descending: true
    });
    if (ApplicationConstants.MinAdsToBeLoaded > 0) {
        query.limit(ApplicationConstants.MinAdsToBeLoaded);
    }
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            const response = results[0];
            response.forEach(ads => {
                const taskKey = ads[db.GoogleNoSql.KEY];
                ads.id = parseInt(taskKey.id);
            });
            callback(response);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Ads list. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

function getDistanceFromLatLonInMiles(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    console.log('The distance in Miles=' + d * 0.62137);
    return d * 0.62137;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

//Fetch Complete Ads count
router.get('/count', function (req, res, next) {

    logs.createLogs("Calling getCompleteAdsCount() method");
    getCompleteAdsCount(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

function getCompleteAdsCount(req, res, callback) {
    logs.createLogs("Entering getCompleteAdsCount() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable);
    db.GoogleNoSql.runQuery(query).then(results => {
        if (results[0] != null) {
            logs.createLogs('getCompleteAdsCount : Ads List fetched Suceessfully');
            const response = results[0].length;
            callback(response);
        }
    }).catch(err => {
        logs.createLogs('Error occured while fetching Ads count. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch active Ads count
router.get('/active/count', function (req, res, next) {

    logs.createLogs("Calling getCompleteAdsCount() method");
    getActiveAdsCount(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

function getActiveAdsCount(req, res, callback) {
    logs.createLogs("Entering getCompleteAdsCount() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdsTable).filter('endDate', '>=', new Date());
    db.GoogleNoSql.runQuery(query).then(results => {
        if (results[0] != null) {
            logs.createLogs('getCompleteAdsCount : Ads List fetched Suceessfully');
            const response = results[0];
            var activeAds= 0;
            response.forEach(ads => {
               if(ads.startDate <= new Date()){
                activeAds++;
               }
            });
            callback(activeAds);
        }
    }).catch(err => {
        logs.createLogs('Error occured while fetching Ads count. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

module.exports = router;