var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
var entity = require('./Entity');
var async = require("async");
var cloudinary = require('../cloudinary/cloudinary');
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');
var jwt = require('jsonwebtoken');
var Cookies = require("cookies");
var sessionstore = require('sessionstore');
var Store = require("jfs");
var jsonStore = new Store("sessionData", {
    type: 'single'
});

//Admin Login
router.post('/login', function (req, res, next) {

    logs.createLogs("Admin Login Api called");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.AdminTable)
        .filter('username', '=', req.body.username)
        .filter('password', '=', req.body.password);
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0].length != 0) {
            const response = results[0];
            const taskKey = response[0][db.GoogleNoSql.KEY];

            // Creating a session Id
            var token = jwt.sign(response[0], ApplicationConstants.secretKeyNameForSession, {
                expiresIn: ApplicationConstants.sessionExpireTime
            });

            new Cookies(req, res).set(ApplicationConstants.shopWalkSessionTokenName, token, {
                httpOnly: true,
                secure: ApplicationConstants.enableSecureToken // for your production environment
            });
            new Cookies(req, res).set(ApplicationConstants.adminId, taskKey.id, {
                httpOnly: true,
                secure: ApplicationConstants.enableSecureToken // for your production environment
            });
            sessionstore.isAdminAuthenticated = true;

            res.status(200);
            res.json({
                isLoginSuccess: true,
                id: parseInt(taskKey.id)
            });

        } else {

            logs.createLogs('Invalid Admin credentials');
            res.status(401);
            res.json({
                isLoginSuccess: false
            });
        }

    }).catch(err => {
        logs.createLogs('Error occured while retailer login. ERROR:', err);
        res.status(500);
        res.json('ERROR:', err);
    });
});


router.post('/logout', function (req, res, next) {

    res.clearCookie(ApplicationConstants.shopWalkSessionTokenName);
    sessionstore.isAdminAuthenticated = false;
    logs.createLogs('Admin Logged Out Successfully');
    res.json('Logged out Successfully');
});


router.get('/isLoggedIn', function (req, res, next) {
    var result;
    if (sessionstore.isAdminAuthenticated) {
        logs.createLogs('user is still loggedin');
        result = true;
    } else {
        logs.createLogs('user is not loggedin');
        result = false;
    }
    res.json({
        isLoggedIn: result
    });
});

module.exports = router;