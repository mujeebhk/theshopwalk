var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
var entity = require('./Entity');
var async = require("async");
var cloudinary = require('../cloudinary/cloudinary');
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');
var jwt = require('jsonwebtoken');
var Cookies = require("cookies");
var sessionstore = require('sessionstore');
var Store = require("jfs");
var jsonStore = new Store("sessionData", {
    type: 'single'
});


//get all cities
router.get('/getCities', function (req, res, next) {

    logs.createLogs("Calling getCitites() method");
    getCities(req, res, function (dbReturnValue) {
        if (res.statusCode == 500) {

            res.status(500);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });
});


//get all cities Db Process
function getCities(req, res, callback) {
    logs.createLogs("Entering getCities() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.CityTable).order('code');
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            const response = results[0];
            response.forEach(city => {
                const taskKey = city[db.GoogleNoSql.KEY];
                city.id = parseInt(taskKey.id);
            });
            callback(response);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching city list. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

module.exports = router;