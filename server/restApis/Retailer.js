var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
var entity = require('./Entity');
var async = require("async");
var cloudinary = require('../cloudinary/cloudinary');
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');


//Create Retailer
router.post('/create', function (req, res, next) {
    logs.createLogs("Calling createRetailer() method");
    createRetailer(req, res, function (dbReturnValue) {
        if (res.statusCode == 500) {

            res.status(500);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });
});

//Create Retailer Db Process
function createRetailer(req, res, callback) {
    logs.createLogs("Entering createRetailer() method");

    req.body.creationDate = new Date();
    req.body.modificationDate = new Date();
    async.doWhilst(function (cb) {
        if (req.body.logopath != null) {

            cloudinary.cloudinaryUpload(req, res, function (cloudinaryRes) {
                if (cloudinaryRes.secure_url != null) {
                    req.body.logoUrl = cloudinaryRes.secure_url;
                    req.body.imageId = cloudinaryRes.public_id;
                    delete req.body.logopath;
                    cb();
                } else {
                    return callback(cloudinaryRes);
                }
            })
        } else {
            req.body.logoUrl = null;
            req.body.imageId = null;
            cb();
        }
    }, function (cb) {

        const taskKey = db.GoogleNoSql.key(ApplicationConstants.RetailerTable);
        const retailerEntity = entity.RetailerEntity(req, taskKey);
        db.GoogleNoSql
            .save(retailerEntity)
            .then(() => {
                logs.createLogs(`Retailer ${taskKey.id} created successfully.`);
                callback({
                    id: parseInt(taskKey.id)
                });
            })
            .catch(err => {
                logs.createLogs('Error occured while creating retailer. ERROR:' + err.stack);
                res.status(500);
                callback('ERROR:' + err.stack);
            });
    });
}

//Update Retailer
router.post('/update', function (req, res, next) {

    logs.createLogs("Calling updateRetailer() method to update Retailer");

    updateRetailer(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Update Retailer Db Process
function updateRetailer(req, res, callback) {
    logs.createLogs("Entering updateRetailer() method");

    //checkForBadRequest
    if (req.body.id.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The id must be a string of 16 characters');
        return callback('Bad Request.The id must be a string of 16 characters');
    }

    async.doWhilst(function (cb) {
        if (req.body.logopath != null) {

            cloudinary.cloudinaryImageUpdate(req, res, function (cloudinaryRes) {
                if (cloudinaryRes.secure_url != null) {
                    delete req.body.logopath;
                    req.body.logoUrl = cloudinaryRes.secure_url;
                    req.body.imageId = cloudinaryRes.public_id;
                    cb();
                } else {
                    return callback(cloudinaryRes);
                }
            })
        } else {
            req.body.logoUrl = null;
            req.body.imageId = null;
            cb();
        }
    }, function (cb) {

        const transaction = db.GoogleNoSql.transaction();
        const taskKey = db.GoogleNoSql.key([ApplicationConstants.RetailerTable, req.body.id]);

        transaction
            .run()
            .then(() => Promise.all([transaction.get(taskKey)]))
            .then(results => {
                const response = results[0];

                if (response[0] != null || response[0] != undefined) {

                    //Configure Update Request
                    req.body.phone = response[0].phone;
                    req.body.creationDate = response[0].creationDate;
                    req.body.modificationDate = new Date();
                    if (req.body.imageId == null) {
                        req.body.logoUrl = response[0].logoUrl;
                        req.body.imageId = response[0].imageId;
                    }

                    const retailerEntity = entity.RetailerEntity(req, taskKey);
                    transaction.save({
                        key: taskKey,
                        data: retailerEntity.data,
                    });
                } else {
                    res.status(400);
                    callback('Invalid Retailer Id');
                }
                return transaction.commit();
            })
            .then(() => {
                // The transaction completed successfully.
                if (res.statusCode != 400) {
                    console.log(`Retailer ${req.body.id} updated successfully.`);
                    callback(`Retailer ${req.body.id} updated successfully.`);
                }
            })
            .catch(err => {
                transaction.rollback()
                logs.createLogs('Error occured while updating retailer. ERROR:' + err.details);
                res.status(500);
                callback('ERROR:' + err.details);
            });

    }); //End of Async
}


//Fetch Retailer by Id
router.get('/getRetailer/:Id', function (req, res, next) {

    logs.createLogs("Calling getRetailerById() method to get retailer");
    getRetailerById(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch Retailer Db Process
function getRetailerById(req, res, callback) {
    logs.createLogs("Entering getRetailerOrBranchById() method");

    //checkForBadRequest
    if (req.params.Id.toString().length != 16) {
        res.status(400);
        logs.createLogs('Bad Request.The _id must be a string of 16 characters');
        return callback('Bad Request.The _id must be a string of 16 characters');
    }

    const transaction = db.GoogleNoSql.transaction();
    const taskKey = db.GoogleNoSql.key([ApplicationConstants.RetailerTable, parseInt(req.params.Id)]);
    transaction
        .run()
        .then(() => Promise.all([transaction.get(taskKey)]))
        .then(results => {

            if (results[0] != null) {
                const response = results[0];
                const taskKey = response[0][db.GoogleNoSql.KEY];
                response[0].id = parseInt(taskKey.id);
                callback(response[0]);

            } else {
                res.status(404);
                callback('Invalid Retailer Id');
            }
            return transaction.commit();

        })
        .then(() => {
            // The transaction completed successfully.
            console.log(`Retailer fetched successfully.`);
        })
        .catch(err => {
            transaction.rollback()
            logs.createLogs('Error occured while fetching retailer. ERROR:' + err.details);
            res.status(500);
            callback('ERROR:' + err.details);
        });
}

//Fetch Retailers List
router.get('/getRetailersList', function (req, res, next) {

    logs.createLogs("Calling getRetailersList() method to get retailer");
    getRetailersList(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch RetailersList Db Process
function getRetailersList(req, res, callback) {
    logs.createLogs("Entering getRetailersList() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.RetailerTable).order('creationDate');
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0] != null) {
            const response = results[0];
            response.forEach(retailer => {
                const taskKey = retailer[db.GoogleNoSql.KEY];
                retailer.id = parseInt(taskKey.id);
            });
            callback(response);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Retailers list. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Check for retailerName
router.get('/retailerNameExists/:retailerName', function (req, res, next) {

    logs.createLogs("Calling retailerNameExists() method to get retailer");
    retailerNameExists(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400 || res.statusCode == 404) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

//Fetch Retailer by Name
function retailerNameExists(req, res, callback) {
    logs.createLogs("Entering retailerNameExists() method");

    var adsResponseList = [];
    const query = db.GoogleNoSql.createQuery(ApplicationConstants.RetailerTable).filter('name', '=', req.params.retailerName);
    db.GoogleNoSql.runQuery(query).then(results => {

        if (results[0].length != 0) {
            callback(true);
        } else {
            callback(false);
        }

    }).catch(err => {
        logs.createLogs('Error occured while fetching Retailer by Name. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

//Fetch Complete Retailer count
router.get('/Count', function (req, res, next) {

    logs.createLogs("Calling getRetailerCount() method");
    getRetailerCount(req, res, function (dbReturnValue) {
        if (res.statusCode == 500 || res.statusCode == 400) {

            res.status(res.statusCode);
            return res.json(dbReturnValue);

        }
        res.status(200);
        res.json(dbReturnValue);


    });

});

function getRetailerCount(req, res, callback) {
    logs.createLogs("Entering getRetailerCount() method");

    const query = db.GoogleNoSql.createQuery(ApplicationConstants.RetailerTable);
    db.GoogleNoSql.runQuery(query).then(results => {
        if (results[0] != null) {
            logs.createLogs('getRetailerCount : Retailer List fetched Suceessfully');
            const response = results[0].length;
            callback(response);
        }
    }).catch(err => {
        logs.createLogs('Error occured while fetching Retailer count. ERROR:' + err.details);
        res.status(500);
        callback('ERROR:' + err.details);
    });
}

module.exports = router;