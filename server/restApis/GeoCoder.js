var express = require('express');
var router = express.Router();
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');
var request = require('request');


//Get LatLong from Address 
router.post('/getLatLongByAddress', function (req, res, next) {
    logs.createLogs("Fetching Lat,Long from address");

    request(ApplicationConstants.GogoleGeoCodeUrl + req.body.address + ApplicationConstants.ApiKey, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var result = JSON.parse(body);
            logs.createLogs("Addrees Co-Ordinates fethed successfully.The Lat Long = " + result.results[0].geometry.location.lat + "," + result.results[0].geometry.location.lng);
            res.json({
                latitude: result.results[0].geometry.location.lat,
                longitude: result.results[0].geometry.location.lng
            });
        } else {
            logs.createLogs("Addrees Co-Ordinates failed to fetch");
            res.status(500);
            return res.json('Error Occured while fetching Addrress Co-ordinates');
        }
    });

});

// get complete google adress details by address
router.post('/getAddressDetailsByLatLng', function (req, res, next) {
    logs.createLogs("Fetching Lat,Long from address");

    request(ApplicationConstants.GogoleReverseGeoCodeUrl + req.body.lat + ',' + req.body.lng + ApplicationConstants.ApiKey, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var result = JSON.parse(body);
            logs.createLogs("Addrees Co-Ordinates fethed successfully.The Lat Long = " + result.results[0].geometry.location.lat + "," + result.results[0].geometry.location.lng);
            res.json({
                formattedAddress: result.results[0].formatted_address,
            });
        } else {
            logs.createLogs("Addrees Co-Ordinates failed to fetch");
            res.status(500);
            return res.json('Error Occured while fetching Addrress Co-ordinates');
        }
    });

});


module.exports = router;