var db = require('../../dbConfig');


function AdminEntity(req, taskKey) {

  const entity = {
    key: taskKey,
    data: [{
        name: 'username',
        value: req.body.username,
      },
      {
        name: 'password',
        value: req.body.password,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}

function CountryEntity(req, taskKey) {

  const entity = {
    key: taskKey,
    data: [{
        name: 'name',
        value: req.body.name,
      },
      {
        name: 'code',
        value: req.body.code,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}

function CityEntity(req, taskKey) {

  const entity = {
    key: taskKey,
    data: [{
        name: 'name',
        value: req.body.name,
      },
      {
        name: 'code',
        value: req.body.code,
      },
      {
        name: 'countyCode',
        value: req.body.countyCode,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}

function RetailerEntity(req, taskKey) {

  const entity = {
    key: taskKey,
    data: [{
        name: 'name',
        value: req.body.name,
      },
      {
        name: 'websiteUrl',
        value: req.body.websiteUrl,
      },
      {
        name: 'phone',
        value: req.body.phone,
      },
      {
        name: 'email',
        value: req.body.email,
      },
      {
        name: 'country',
        value: req.body.country,
      },
      {
        name: 'logoUrl',
        value: req.body.logoUrl,
      },
      {
        name: 'imageId',
        value: req.body.imageId,
      },
      {
        name: 'country',
        value: req.body.country,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}

function AdsEntity(req, taskKey) {
  const entity = {
    key: taskKey,
    data: [{
        name: 'masterId',
        value: req.body.masterId,
      }, {
        name: 'retailerId',
        value: req.body.retailerId,
      },
      {
        name: 'storeId',
        value: req.body.storeId,
      },
      {
        name: 'retailerName',
        value: req.body.retailerName,
      },
      {
        name: 'retailerLogoUrl',
        value: req.body.retailerLogoUrl,
      },
      {
        name: 'storeDetails',
        value: req.body.storeDetails,
      },
      {
        name: 'shortText',
        value: req.body.shortText,
      },
      {
        name: 'longText',
        value: req.body.longText,
      },
      {
        name: 'category',
        value: req.body.category,
      },
      {
        name: 'startDate',
        value: req.body.startDate,
      },
      {
        name: 'endDate',
        value: req.body.endDate,
      },      
      {
        name: 'fileType',
        value: req.body.fileType,
      },
      {
        name: 'adImageUrls',
        value: req.body.adImageUrls,
      },
      {
        name: 'adPdf',
        value: req.body.adPdf,
      },
      // {
      //   name: 'status',
      //   value: req.body.status,
      // },
      {
        name: 'createdBy',
        value: req.body.createdBy,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}


function StoreEntity(req, taskKey) {
  const coordinates = {
    latitude: req.body.latitude,
    longitude: req.body.longitude
  };
  const geoPoint = db.GoogleNoSql.geoPoint(coordinates);

  const entity = {
    key: taskKey,
    data: [{
        name: 'retailerId',
        value: req.body.retailerId,
      },
      {
        name: 'storeName',
        value: req.body.storeName,
      },
      {
        name: 'address',
        value: req.body.address,
      },
      {
        name: 'city',
        value: req.body.city,
      },
      {
        name: 'landmark',
        value: req.body.landmark,
      },
      {
        name: 'phone',
        value: req.body.phone,
      },
      {
        name: 'storeTimings',
        value: req.body.storeTimings,
      },
      {
        name: 'location',
        value: geoPoint,
      },
      {
        name: 'creationDate',
        value: req.body.creationDate,
      },
      {
        name: 'modificationDate',
        value: req.body.modificationDate,
      }
    ]
  };

  return entity;
}

module.exports.AdminEntity = AdminEntity;
module.exports.RetailerEntity = RetailerEntity;
module.exports.AdsEntity = AdsEntity;
module.exports.StoreEntity = StoreEntity;
module.exports.CityEntity = CityEntity;
module.exports.CountryEntity = CountryEntity;