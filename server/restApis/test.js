var express = require('express');
var router = express.Router();
var db = require('../../dbConfig');
const Storage = require('@google-cloud/storage');
const Datastore = require('@google-cloud/datastore');
// Your Google Cloud Platform project ID
const projectId = 'shopwalktest';

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
  keyFilename: './shopWalkTest-93e4af656fe6.json'
});
const storage = new Storage({
  keyFilename: './shopWalkTest-93e4af656fe6.json'
});

router.post('/testGoogleNoSql', function (req, res, next) {
  addTask('test', function (resp) {

    res.json(resp);
  })
});

function credentials() {
  // Makes an authenticated API request.
  storage
    .getBuckets()
    .then((results) => {
      const buckets = results[0];

      console.log('Buckets:');
      buckets.forEach((bucket) => {
        console.log(bucket.name);
      });
    })
    .catch((err) => {
      console.error('ERROR:', err);
    });
}

//Google Nosql Insert
//Task=Table, Entity={columnName : Values}
function addTask(description, callback) {
  const taskKey = datastore.key('Ads');//Table Name Ads

  const coordinates = {
    latitude: 40.6894,
    longitude: -74.0447
  };
  
  const geoPoint = datastore.geoPoint(coordinates);

  const entity = {
    key: taskKey,
    data: [{
        name: '_id',
        value: '5aec3046cdca9c4b619f73e5',
      },
      {
        name: 'longitude',
        value: 78.45645645,
        excludeFromIndexes: true,
      },
      {
        name: 'adText',
        value: '1000% Offer',
      },
      {
        name: 'expiryDate',
        value: '2018-05-03T10:53:36.612Z',
      },
      {
        name: 'geoPoint',
        value: geoPoint,
      },

    ],
  };

  datastore
    .save(entity)
    .then(() => {
      console.log(`Task ${taskKey.id} created successfully.`);
      callback(`Task ${taskKey.id} created successfully.`);
    })
    .catch(err => {
      console.error('ERROR:', err);
      callback('ERROR:', err);
    });
}

//Update
router.post('/updateNoSql', function (req, res, next) {
  updateTask(req,'test', function (resp) {

    res.json(resp);
  })
});


function updateTask(req,description, callback) {

  const transaction = datastore.transaction();
  const taskKey = datastore.key(['Ads', req.body.id]);

  var data= [
  {
    name: 'adText',
    value: '99.9999% Offer DShab',
  },
  {
    name: 'expiryDate',
    value: '2018-05-03T10:53:36.612Z',
  },

]

  transaction
    .run()
    .then(() => Promise.all([transaction.get(taskKey)]))
    .then(results => {
      const task = results[0];
      //task.adText = 'ShabTest';
      transaction.save({
        key: taskKey,
        data: data,
      });
      return transaction.commit();
    })
    .then(() => {
      // The transaction completed successfully.
      console.log(`Task ${req.body.id} updated successfully.`);
      callback(`Task ${req.body.id} updated successfully.`);
    })
    .catch(() => transaction.rollback());
  
}

//Update
router.post('/updateManyNoSql', function (req, res, next) {
  updateMany(req,'test', function (resp) {

    res.json(resp);
  })
});


function updateMany(req,description, callback) {

  const transaction = datastore.transaction();
  const taskKey = datastore.key(['Task', req.body.id]);

  const query = datastore
  .createQuery('Task')
  .filter('done', '=', false);

  var data= [
  {
    name: 'adText',
    value: '99.9999% Offer DShab',
  }

]

  transaction
    .run()
    .then(() => datastore.runQuery(query))
    .then(results => {
     
      const tasks = results[0];
  
    console.log('Tasks:');
    tasks.forEach(task => {
        const taskKey = task[datastore.KEY];
        console.log(taskKey.id, task);

        transaction.save({
          key: taskKey,
          data: data,
        });
      });
      return transaction.commit();
    })
    .then(() => {
      // The transaction completed successfully.
      console.log(`Task ${req.body.id} updated successfully.`);
      callback(`Task ${req.body.id} updated successfully.`);
    })
    .catch((err) => {
      transaction.rollback()
      console.log('Error Occured= ' +err);
    }) ;
  
}

router.get('/getFromGoogle', function (req, res, next) {
  getData('test', function (resp) {

    res.json(resp);
  })
});

//Google Nosql Fetch
//Task=Table, Entity={columnName : Values}
function getData(description, callback) {


  const coordinates = {
    latitude: 41.6894,
    longitude: -175.0447
  };
  
  const geoPoint = datastore.geoPoint(coordinates);

  const taskKey = datastore.key('Ads');
  const query = datastore
  .createQuery('Ads')
  .filter('geoPoint', '<=', geoPoint);

  datastore.runQuery(query).then(results => {
    // Task entities found.
    const tasks = results[0];
  
    console.log('Tasks:');
    tasks.forEach(task => {
        const taskKey = task[datastore.KEY];
        console.log(taskKey.id, task);
        task.id =taskKey.id;
      });
    callback(results);
  });

}

router.get('/getFromGoogleById/:id', function (req, res, next) {
  getDataById(req, function (resp) {

    res.json(resp);
  })
});

function getDataById(req, callback) {
  const taskKey1 = datastore.key('Retailer');
  taskKey1.id=req.params.id;
  const taskKey = datastore.key(['Retailer', taskKey1.id]);
  db.GoogleNoSql.get(taskKey1).then(results => {
  // Task found.

  const tasks = results[0];
  
  console.log('Tasks:');
  const taskKey = tasks[datastore.KEY];
  console.log(taskKey.id, tasks);
  tasks.id =taskKey.id;

  const entity = results[0];
  console.log(entity);
  callback(entity);
});

}


































/* GET home page. */
router.get('/testIndex', function (req, res, next) {
  console.log('Api Test')
  res.json({
    title: 'Express Olegaa'
  });
});

router.post('/insert', function (req, res, next) {
  //res.json('index', { title: 'Express' });

  console.log('Insert test');

  var dbo = db.getTestDb();
  var myobj = [{
      name: 'John',
      address: 'Highway 71',
      "tags": [
        "music",
        "cricket",
        "blogs"
      ]
    },
    {
      name: 'Peter',
      address: 'Lowstreet 4',
      "tags": [
        "music",
        "cricket",
        "blogs"
      ]
    },
    {
      name: 'Amy',
      address: 'Apple st 652',
      "tags": [
        "music",
        "cricket",
        "blogs"
      ]
    },
    {
      name: 'Hannah',
      address: 'Mountain 21',
      "tags": [
        "music",
        "cricket",
        "blogs", "movies"
      ]
    },
    {
      name: 'Michael',
      address: 'Valley 345'
    },
    {
      name: 'Sandy',
      address: 'Ocean blvd 2'
    },
    {
      name: 'Betty',
      address: 'Green Grass 1'
    },
    {
      name: 'Richard',
      address: 'Sky st 331',
      "tags": [
        "music",
        "cricket",
        "blogs"
      ]
    },
    {
      name: 'Susan',
      address: 'One way 98'
    },
    {
      name: 'Vicky',
      address: 'Yellow Garden 2'
    },
    {
      name: 'Ben',
      address: 'Park Lane 38'
    },
    {
      name: 'William',
      address: 'Central st 954',
      "tags": [
        "music",
        "cricket",
        "blogs", "movies"
      ]
    },
    {
      name: 'Chuck',
      address: 'Main Road 989'
    },
    {
      name: 'Viola',
      address: 'Sideway 1633'
    }
  ];
  dbo.collection("testMongo").insertMany(myobj, function (err, res1) {
    if (err) {
      //throw err;
      console.log(err);
      return res.json(err);
    }
    console.log("Number of documents inserted: " + res1.insertedCount);

    res.json(res1);
  })
});

module.exports = router;