var cloudinary = require('cloudinary');
var Jimp = require("jimp");
var async = require("async");
var logs = require('../config/logsHandler');
var ApplicationConstants = require('../common/ApplicationConstants');

//Cloudinary Configuration
cloudinary.config({
    cloud_name: ApplicationConstants.Cloudinary_Cloud_Name,
    api_key: ApplicationConstants.Cloudinary_Api_Key,
    api_secret: ApplicationConstants.Cloudinary_Api_Secret
});

function cloudinaryImageDelete(req, res, callback) {

    if (req.body.imageId != null) {
        cloudinary.v2.uploader.destroy(req.body.imageId, {
                invalidate: true
            },
            function (error, result) {
                if (error) {
                    logs.createLogs('Delete Error' + error)
                }
                console.log(result)
            });
    }
}

function cloudinaryImageUpdate(req, res, callback) {

    if (req.body.imageId != null) {
        cloudinary.v2.uploader.destroy(req.body.imageId, {
                invalidate: true
            },
            function (error, result) {
                if (error) {
                    logs.createLogs('Delete Error' + error)
                }
                console.log(result)
            });
    }

    cloudinaryUpload(req, res, function (uploadResponse) {
        callback(uploadResponse);
    });
}

function cloudinaryUpload(req, res, callback) {
    logs.createLogs('cloudinaryUpload()  called');
    var base64String = req.body.logopath.replace(/^data:image\/\w+;base64,/, "");
    async.doWhilst(function (cb) {

        //compressImage(base64String, logger, function (err, image) {req.body.logoPath = image;cb();});
        cb();
    }, function (cb) {

        cloudinary.v2.uploader.upload(req.body.logopath, function (error, result) {
            if (error) {
                logs.createLogs('Error Occured while uploading image to cloudinary.The Error = ' + error);
                return callback(error);
            }

            logs.createLogs('Image uploaded succesfully .The response url= ' + result.secure_url);
            callback(result);
        });
    });
}

function adPdfOrImageUpload(req, res, callback) {
    logs.createLogs('adPdfOrImageUpload()  called');

    //Deleting Old Images
    if (req.body.adImageUrls != null) {
        logs.createLogs('Deleting Old Images While Update');
        for (var i = 0; i < req.body.adImageUrls.length; i++) {

            cloudinary.v2.uploader.destroy(req.body.adImageUrls[i].imageId, {
                    invalidate: true
                },
                function (error, result) {
                    if (error) {
                        logs.createLogs('Delete Error' + error)
                    }
                    console.log(result)
                });

        }
    }

    var initialUpload = [
        function (cb) {

            cloudinary.v2.uploader.upload(req.body.logopath, function (error, result) {
                if (error) {
                    logs.createLogs('Error Occured while uploading image/pdf to cloudinary.The Error = ' + error);
                    return callback(error);
                }

                req.body.uploadedFileUrl = result.secure_url;
                req.body.filePublicId = result.public_id;
                if (result.pages) {
                    req.body.filePages = result.pages;
                }
                logs.createLogs('Uploaded pdf or ad image .The response url= ' + result.secure_url);
                cb();
            });
        },
        function (cb) {

            req.body.pageToUpload = 1;
            mulltipleTransformationUpload(req, res, function (cloudinaryRes) {
                return callback(cloudinaryRes);
            });
            cb();
        }
    ];

    async.series(initialUpload, (err, results) => {
        if (err) {
            logs.createLogs('Error occureed at async.series' + err);
            return next(err);
        }
    });
}

function mulltipleTransformationUpload(req, res, callback) {

    logs.createLogs('mulltipleTransformationUpload()  called');
    var uploadedUrls = [];
    var pdfImagesList = [];

    var multipleImageUpload = [
        function (cb) {

            if ((req.body.uploadedFileUrl.substring(req.body.uploadedFileUrl.length - 4)) == ".pdf") {
                cloudinary.v2.uploader.upload(req.body.uploadedFileUrl, {
                    format: 'jpg',
                    page: req.body.pageToUpload
                }, function (error, result) {
                    if (error) {
                        logs.createLogs('Error Occured while uploading pdf as image to cloudinary.The Error = ' + error);
                        return callback(error);
                    }

                    req.body.uploadedImageUrl = result.secure_url;
                    uploadedUrls.push({
                        imageUrl: result.secure_url,
                        imageId: result.public_id,
                        type: "original"
                    });
                    logs.createLogs('Uploaded original image .The response url= ' + result.secure_url);
                    cb();
                });
            } else {
                req.body.uploadedImageUrl = req.body.uploadedFileUrl;
                uploadedUrls.push({
                    imageUrl: req.body.uploadedFileUrl,
                    imageId: req.body.filePublicId,
                    type: "original"
                });
                logs.createLogs('Uploaded original image .The response url= ' + req.body.uploadedFileUrl);
                cb();
            }
        },
        function (cb) {

            if ((req.body.uploadedImageUrl.substring(req.body.uploadedImageUrl.length - 4)) != ".pdf") {
                cloudinary.v2.uploader.upload(req.body.uploadedImageUrl, {
                    width: 400
                }, function (error, result) {
                    if (error) {
                        logs.createLogs('Error Occured while uploading image to cloudinary.The Error = ' + error);
                        return callback(error);
                    }

                    uploadedUrls.push({
                        imageUrl: result.secure_url,
                        imageId: result.public_id,
                        type: "optimized1"
                    });
                    logs.createLogs('Uploaded optimized image with width=400 .The response url= ' + result.secure_url);
                    cb();
                });
            } else {
                cb();
            }
        },
        function (cb) {

            if ((req.body.uploadedImageUrl.substring(req.body.uploadedImageUrl.length - 4)) != ".pdf") {
                cloudinary.v2.uploader.upload(req.body.uploadedImageUrl, {
                    width: 300
                }, function (error, result) {
                    if (error) {
                        logs.createLogs('Error Occured while uploading image to cloudinary.The Error = ' + error);
                        return callback(error);
                    }

                    uploadedUrls.push({
                        imageUrl: result.secure_url,
                        imageId: result.public_id,
                        type: "optimized2"
                    });
                    logs.createLogs('Uploaded optimized image with width=300.The response url= ' + result.secure_url);
                    logs.createLogs('Leaving mulltipleTransformationUpload()');
                    cb();
                });
            } else {
                logs.createLogs('Request has pdf base64 url');
                cb();
            }
        },
        function (cb) {
            if ((req.body.uploadedFileUrl.substring(req.body.uploadedFileUrl.length - 4)) == ".pdf") {

                for (var j = 0; j < uploadedUrls.length; j++) {
                    uploadedUrls[j].pdfPageNumber = req.body.pageToUpload;
                    pdfImagesList.push(uploadedUrls[j]);
                }

                req.body.pageToUpload = req.body.pageToUpload + 1;
                cb();
            } else {
                cb();
            }
        }
    ]

    var pageCount = [];
    if (req.body.fileType === "application/pdf") {
        for (var i = 0; i < req.body.filePages; i++) {
            pageCount.push(i + 1);
        }
    } else {
        pageCount.push(1);
    }

    async.forEachSeries(pageCount, function (pageNo, callback_s1) {
        console.log(pageNo);
        async.series(multipleImageUpload, (err, results) => {
            if (err) {
                logs.createLogs('Error occureed at async.series' + err);
                return next(err);
            }
            if ((req.body.uploadedFileUrl.substring(req.body.uploadedFileUrl.length - 4)) == ".pdf") {
                if (pageNo == req.body.filePages) {
                    logs.createLogs('Upload Results = ' + JSON.stringify(pdfImagesList));
                    callback(pdfImagesList);
                }
            } else {
                logs.createLogs('Upload Results = ' + JSON.stringify(uploadedUrls));
                callback(uploadedUrls);
            }
            uploadedUrls = [];
            callback_s1();
        });
    });
}


function compressImage(base64String, logger, callback) {
    logger.debug('The compressImage() method called inside cloudinary.js')
    var imgBuffer = new Buffer(base64String, 'base64');

    // Image gets compresses using Jimp
    Jimp.read(imgBuffer, function (err, image) {
        if (err) {
            logs.createLogs('Error occured while converting the  buffer to Jimapimage object in compressImage() method inside cloudinary.js' + err);
            return callback(err, null);
        }
        var imageHeight = image.bitmap.height;
        var imageWidth = image.bitmap.width;
        if (imageHeight > 400 && imageWidth > 400) {
            image.resize(400, 400); // resize
        }
        image.getBase64(Jimp.AUTO, function (err, compressedBase64Image) {
            if (err) {
                logs.createLogs('Error occured while converting the  buffer to Jimapimage object in compressImage() method inside cloudinary.js' + err);
                return callback(err, null);
            }

            callback(err, compressedBase64Image);
        });

    });
};


module.exports.cloudinaryUpload = cloudinaryUpload;
module.exports.cloudinaryImageUpdate = cloudinaryImageUpdate;
module.exports.adPdfOrImageUpload = adPdfOrImageUpload;