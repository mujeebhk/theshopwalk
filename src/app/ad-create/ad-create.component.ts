import { Component, OnInit } from '@angular/core';
import { StoreService } from '../service/store.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RetailerDetails } from '../model/retailerDetails';
import { StoreDetails } from '../model/storeDetails';
import { AdDetails } from '../model/adDetails';
import { RetailerService } from '../service/retailer.service';
import { AdService } from '../service/ad.service';
import { AdImageUrl } from '../model/AdImageUrl';
import { AdPDF } from '../model/AdPDF';
import { Geolocation } from '../model/location';
import { map } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { v4 as uuid } from 'uuid';
import { SlimLoadingBarService } from '../../../node_modules/ng2-slim-loading-bar';
import { UrlHelper } from '../common/helpers/url.helper';
import { DynamicHelper } from '../common/helpers/dynamic.helper';

@Component({
  selector: 'app-ad-create',
  templateUrl: './ad-create.component.html',
  styleUrls: ['./ad-create.component.scss']
})
export class AdCreateComponent implements OnInit {
  retailerDetails: RetailerDetails[] = [];
  storeDetails: StoreDetails[] = [];
  adsData: {
    masterId: string, retailerId: number, storeId: number, retailerName: string, retailerLogoUrl: string,
    shortText: string, longText: string, startDate: Date, endDate: Date,
    category: string, logopath: string, adPdf: string, createdBy: string, storeDetails: StoreDetails, fileType: string
  }[] = [];
  shortText: string;
  longText: string;
  adName: string;
  adLocation: Geolocation = new Geolocation();
  startDate: Date;
  endDate: Date;
  category: string;
  logopath: string;

  fileType: string;
  adDetails: AdDetails = new AdDetails();
  isSuccess: boolean = false;
  showMsg: boolean = false;
  retailerId: number;
  storeId: number;
  navigateUrl: string;
  visible: boolean = false;
  files: any;
  isValidImgSize: boolean = true;
  imageName: string;
  selectedRetailer: RetailerDetails = new RetailerDetails();
  selectedStores: StoreDetails[] = [];
  isValidStartDate: boolean = true;
  isValidDate: boolean = true;
  masterId: string;

  retailerControl = new FormControl('', [Validators.required]);
  storeControl = new FormControl('', [Validators.required]);

  constructor(private slimLoadingBarService: SlimLoadingBarService, private retailerService: RetailerService, private storeService: StoreService, private adService: AdService, private router: Router, private route: ActivatedRoute) {
    this.navigateUrl = "adverts";
    this.isSuccess = true;
    this.retailerId = -1;
    this.storeId = -1;
    this.masterId = uuid();
  }

  ngOnInit() {
  }

  public OnSave() {
    this.slimLoadingBarService.start();
    this.prepareData();
    this.adsData.forEach((data) => {
      // console.log(data);
      this.adService.createAd(data).subscribe((data) => {
        this.isSuccess = true;
        this.showMsg = true;
        this.slimLoadingBarService.complete();
        setTimeout(() => {
          this.visible = false;
          this.router.navigate([this.navigateUrl]);
        }, 4000)
      }, (e) => {
        this.isSuccess = false;
        this.showMsg = true;
        this.slimLoadingBarService.complete();
      });
    });
  }

  public OnCancel() {
    this.clear();
    this.router.navigate([this.navigateUrl]);
    this.visible = false;
  }

  public clear() {
    // this.adData = {
    //   masterId: "", retailerId: null, storeId: null, retailerName: "", retailerLogoUrl: "",
    //   shortText: "", longText: "", startDate: null, endDate: null,
    //   category: "", logopath: "", adPdf: "", createdBy: "", storeDetails: new StoreDetails(), fileType: ""
    // };
  }

  open() {
    this.visible = true;
    this.retailerService.getRetailerList().subscribe((data) => {
      this.retailerDetails = data.sort(DynamicHelper.dynamicSort('name'));
      this.retailerDetails[0].id
    });
  }

  OnRetailerSelect(retailerdtls: RetailerDetails) {
    this.selectedRetailer = retailerdtls;
    this.storeService.getStoreListByRetailerId(retailerdtls.id).subscribe((data) => {
      this.storeDetails = data.sort(DynamicHelper.dynamicSort('storeName'));
    });
  }

  getFiles(event) {
    this.isValidImgSize = true;
    this.files = event.target.files;
    if (this.files && this.files[0]) {
      var fileSize = this.files ? (this.files[0] ? this.files[0].size / 1024 / 1024 : 0) : 0;
      this.fileType = this.files ? (this.files[0] ? this.files[0].type : 0) : 0;
      if (fileSize <= 4) {
        this.isValidImgSize = true;
        // this._handleReaderLoaded.bind(this);
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(this.files[0]);
      }
      else {
        this.isValidImgSize = false;
      }
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.logopath = btoa(binaryString);  // Converting binary string data.
    if (this.fileType.includes("image")) {
      this.logopath = "data:image;base64," + this.logopath;  // Converting binary string data.
    }
    else if (this.fileType.includes("pdf")) {
      this.logopath = "data:application/pdf;base64," + this.logopath;  // Converting binary string data.
    }
    // console.log(this.logopath);
  }

  onSelectOfStores(data: StoreDetails) {
    var index = this.selectedStores.findIndex(s => s.id == data.id);
    if (index < 0) {
      this.selectedStores.push(data);
    }
    else {
      this.selectedStores.splice(index, 1);
    }
    console.log(this.selectedStores);
  }

  prepareData() {
    this.selectedStores.forEach((item) => {
      let adData: {
        masterId: string, retailerId: number, storeId: number, retailerName: string, retailerLogoUrl: string,
        shortText: string, longText: string, startDate: Date, endDate: Date,
        category: string, logopath: string, adPdf: string, createdBy: string, storeDetails: StoreDetails, fileType: string
      }
        = {
        masterId: "", retailerId: null, storeId: null, retailerName: "", retailerLogoUrl: "",
        shortText: "", longText: "", startDate: null, endDate: null,
        category: "", logopath: "", adPdf: "", createdBy: "", storeDetails: new StoreDetails(), fileType: ""
      };
      adData.masterId = this.masterId;
      adData.retailerId = this.selectedRetailer.id;
      adData.retailerName = this.selectedRetailer.name;
      adData.retailerLogoUrl = this.selectedRetailer.logoUrl;
      adData.storeId = item.id;
      adData.storeDetails = item;
      adData.shortText = this.shortText ? this.shortText : "";
      adData.longText = this.longText ? this.longText : "";
      adData.logopath = this.logopath;
      adData.category = this.category ? this.category : "";
      adData.startDate = this.startDate ? this.startDate : new Date();
      adData.endDate = this.endDate ? this.endDate : new Date();
      adData.fileType = this.fileType;
      // push to array
      this.adsData.push(adData);
    });
  }

  OnDateChange() {
    if ((!this.startDate && this.endDate)) {
      this.isValidStartDate = false;
    }
    else {
      this.isValidStartDate = true;
    }

    if (this.startDate > this.endDate) {
      this.isValidDate = false;
    }
    else {
      this.isValidDate = true;
    }
  }
}
