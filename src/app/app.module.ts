import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CustomFormsModule } from 'ng2-validation'
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatDialogModule, MatInputModule, MatRadioModule, MatTableModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatSelectModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AdDetailsComponent } from './ad-details/ad-details.component';
import { AdService } from './service/ad.service';
import { HttpModule } from '@angular/http';
import { MainGalleryComponent } from './main-gallery/main-gallery.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RetailerHomeComponent } from './retailer-home/retailer-home.component';
import { RetailerCreateComponent } from './retailer-create/retailer-create.component';
import { AdCreateComponent } from './ad-create/ad-create.component';
import { RetailerService } from './service/retailer.service';
import { StoreService } from './service/store.service';
import { RetailerUpdateComponent } from './retailer-update/retailer-update.component';
import { StoreHomeComponent } from './store-home/store-home.component';
import { AdUpdateComponent } from './ad-update/ad-update.component';
import { StoreCreateComponent } from './store-create/store-create.component';
import { StoreUpdateComponent } from './store-update/store-update.component';
import { AdHomeComponent } from './ad-home/ad-home.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { RetailerViewComponent } from './retailer-view/retailer-view.component';
import { StoreViewComponent } from './store-view/store-view.component';
import { AdViewComponent } from './ad-view/ad-view.component';
import { LayoutModule } from '@angular/cdk/layout';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { AdListComponent } from './ad-list/ad-list.component';
import { SearchFilterPipe } from './common/pipe/searchFilterPipe';
import { CityService } from './service/city.service';
import { CountryService } from './service/country.service';
import { AdChangeStatusDialogComponent } from './ad-change-status-dialog/ad-change-status-dialog.component';
import { AdminService } from './service/admin.service';
import { AdViewImagesComponent } from './ad-view-images/ad-view-images.component';
import { OnlyLoggedInUsersGuard } from './common/OnlyLoggedInUsersGuard';
import { LocationChangeDialogComponent } from './location-change-dialog/location-change-dialog.component';
import { FooterComponent } from './footer/footer.component'
import { GeoCoderService } from './service/geoCoder.service';
@NgModule({
  declarations: [
    AppComponent,
    MainGalleryComponent,
    AdDetailsComponent,
    LoginComponent,
    RetailerHomeComponent,
    RetailerCreateComponent,
    AdCreateComponent,
    RetailerUpdateComponent,
    StoreHomeComponent,
    AdUpdateComponent,
    StoreCreateComponent,
    StoreUpdateComponent,
    AdHomeComponent,
    AdminHeaderComponent,
    RetailerViewComponent,
    StoreViewComponent,
    AdViewComponent,
    AdminDashboardComponent,
    DashboardDetailsComponent,
    AdListComponent,
    SearchFilterPipe,
    AdChangeStatusDialogComponent,
    AdViewImagesComponent,
    LocationChangeDialogComponent,
    FooterComponent
  ],
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'home',
        component: MainGalleryComponent
      },
      {
        path: 'adDetails/:id',
        component: AdDetailsComponent
      },
      {
        path: 'tswlogin',
        component: LoginComponent,
      },
      {
        path: 'dashboard',
        component: DashboardDetailsComponent,
        canActivate: [OnlyLoggedInUsersGuard]
      },
      {
        path: 'retailers',
        component: RetailerHomeComponent,
        canActivate: [OnlyLoggedInUsersGuard]
      },
      {
        path: 'adverts',
        component: AdListComponent,
        canActivate: [OnlyLoggedInUsersGuard]
      },
      {
        path: 'footer',
        component: FooterComponent
      }
      // { path: 'error', component: MainGalleryComponent },
      // { path: '**', redirectTo: 'home' }
    ], {
        useHash: false
      }),
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    HttpModule,
    FormsModule,
    MatTableModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    ReactiveFormsModule,
    SlimLoadingBarModule.forRoot(),
    AngularFontAwesomeModule,
    CustomFormsModule,
    MatDialogModule,
    MatRadioModule,
    MatInputModule,
    GooglePlaceModule
  ],
  providers: [
    AdService,
    RetailerService,
    StoreService,
    CityService,
    AdminService,
    CountryService,
    GeoCoderService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useValue: {} },
    OnlyLoggedInUsersGuard
  ],
  entryComponents: [AdHomeComponent, AdChangeStatusDialogComponent, LocationChangeDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
