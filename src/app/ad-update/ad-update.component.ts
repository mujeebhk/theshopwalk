import { Component, OnInit } from '@angular/core';
import { RetailerDetails } from '../model/retailerDetails';
import { StoreDetails } from '../model/storeDetails';
import { AdDetails } from '../model/adDetails';
import { RetailerService } from '../service/retailer.service';
import { StoreService } from '../service/store.service';
import { AdService } from '../service/ad.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-update',
  templateUrl: './ad-update.component.html',
  styleUrls: ['./ad-update.component.scss']
})
export class AdUpdateComponent implements OnInit {
  public retailerDetails: RetailerDetails = new RetailerDetails();
  public storeDetails: StoreDetails = new StoreDetails();
  // public adData: {
  //   retailerId: number, storeId: number, retailerName: string, retailerLogoUrl: string, adName: string,
  //   adText: string, location: Geolocation, distance: number, startDate: Date, endDate: Date,
  //   category: string, logo: string, adPdf: string, createdBy: string, storeDetails: StoreDetails
  // }
  //   = {
  //     retailerId: null, storeId: null, retailerName: "", retailerLogoUrl: "", adName: "",
  //     adText: "", location: new Geolocation(), distance: null, startDate: null, endDate: null,
  //     category: "", logo: "", adPdf: "", createdBy: "", storeDetails: new StoreDetails()
  //   };
  public adDetails: AdDetails = new AdDetails();
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  public retailerId: number;
  public storeId: number;
  public adId: number;
  public navigateUrl: string;

  constructor(private retailerService: RetailerService, private storeService: StoreService, private adService: AdService, private router: Router, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.retailerId = +params['rid']; // (+) converts string 'id' to a number
    });
    this.route.params.subscribe(params => {
      this.storeId = +params['sid']; // (+) converts string 'id' to a number
    });
    this.route.params.subscribe(params => {
      this.adId = +params['aid']; // (+) converts string 'id' to a number
    });
    this.navigateUrl = "retailer/" + this.retailerId + "/store/" + this.storeId + "/ads";
    this.isSuccess = true;

    this.adService.getAdDetailsById(this.adId).subscribe((data) => {
      this.adDetails = data;
    });
  }

  ngOnInit() {
  }

  public OnSave() {
    this.adService.updateAd(this.adDetails).subscribe((data) => {
      this.isSuccess = true;
      this.showMsg = true;
      setTimeout(() => {
        this.router.navigate([this.navigateUrl]);
      }, 4000)
    }, (error) => {
      this.isSuccess = false;
      this.showMsg = true;
    });
  }

  public OnCancel() {
    this.clear();
    this.router.navigate([this.navigateUrl]);
  }

  public clear() {
    // this.adData = {
    //   retailerId: null, storeId: null, retailerName: "", retailerLogoUrl: "", adName: "",
    //   adText: "", location: new Geolocation(), distance: null, startDate: null, endDate: null,
    //   category: "", logo: "", adPdf: "", createdBy: "", storeDetails: new StoreDetails()
    // };
    this.adDetails = new AdDetails();
  }
}
