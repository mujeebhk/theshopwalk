import { IStoreDetails } from './IStoreDetails';
import { Geolocation } from "./location";

export class StoreDetails {
  address: string;
  landmark: string;
  latitude: number;
  longitude: number;
  phone: number;
  retailerId: number;
  storeName: string;
  storeTimings: string;
  id: number;
  creationDate: Date;
  modificationDate: Date;
  city: string;
  location: Geolocation;

  constructor(rawData?: IStoreDetails) {
    if (rawData) {
      this.address = rawData.address;
      this.landmark = rawData.landmark;
      this.latitude = rawData.latitude;
      this.longitude = rawData.longitude;
      this.phone = rawData.phone;
      this.retailerId = rawData.retailerId;
      this.storeName = rawData.storeName;
      this.storeTimings = rawData.storeTimings;
      this.id = rawData.id;
      this.creationDate = rawData.creationDate;
      this.modificationDate = rawData.modificationDate;
      this.city = rawData.city;
      this.location = rawData.location;
    }
    else {
      this.address = "";
      this.landmark = "";
      this.latitude = 0;
      this.longitude = 0;
      this.phone = 0;
      this.retailerId = 0;
      this.storeName = "";
      this.storeTimings = "";
      this.id = 0;
      this.creationDate = new Date();
      this.modificationDate = new Date();
      this.city = "";
      this.location = new Geolocation();
    }
  }
}