export interface ICountry {
    name: string;
    code: string; 
    creationDate: Date ;
    modificationDate: Date ;
}