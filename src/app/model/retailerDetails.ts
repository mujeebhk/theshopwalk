import { IRetailerDetails } from "./IretailerDetails";

export class RetailerDetails {
    logoUrl: string;
    logopath: string;
    imageId: string;
    modificationDate: Date;
    phone: number;
    websiteUrl: string;
    creationDate: Date;
    email: string;
    name: string;
    id: number;
    country: string;

    constructor(rawData?: IRetailerDetails) {
        if (rawData) {
            this.logoUrl = rawData.logoUrl;
            this.imageId = rawData.imageId;
            this.modificationDate = rawData.modificationDate;
            this.phone = rawData.phone;
            this.websiteUrl = rawData.websiteUrl;
            this.creationDate = rawData.creationDate;
            this.email = rawData.email;
            this.name = rawData.name;
            this.id = rawData.id;
            this.country = rawData.country;
            this.logopath = rawData.logopath;
        }
        else {
            this.logoUrl = "";
            this.imageId = "";
            this.modificationDate = new Date();
            this.phone = 0;
            this.websiteUrl = "";
            this.creationDate = new Date();
            this.email = "";
            this.name = "";
            this.id = 0;
            this.country = "";
            this.logopath = "";
        }
    }
}