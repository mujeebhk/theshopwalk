import { AdImageUrl } from "./AdImageUrl";
import { AdPDF } from "./AdPDF";
import { StoreDetails } from "./storeDetails";
import { Geolocation } from "./location";


export interface IAdDetails {
    id: number;
    shortText: string;
    longText: string;
    category: string;
    adImageUrls: AdImageUrl[];
    adPdf: AdPDF;
    retailerId: number;
    retailerLogoUrl: string;
    retailerName: string;
    storeId: number;
    storeDetails: StoreDetails;
    distance: number;
    startDate: Date;
    createdBy: string;
    creationDate: Date;
    endDate: Date;
    logoPath: string;
    validityInDays: number;
    coverimgUrlForLs: string;
    coverimgUrlForXs: string;
    coverimgUrlForMs: string;
    masterId: string;
    fileType: string;
}