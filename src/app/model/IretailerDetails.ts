export interface IRetailerDetails {
    logoUrl: string;
    imageId: string;
    modificationDate: Date;
    phone: number;
    websiteUrl: string;
    creationDate: Date;
    email: string;
    name: string;
    id: number;
    country: string;
    logopath: string;
}