import { AdImageUrl } from "./AdImageUrl";
import { AdPDF } from "./AdPDF";
import { StoreDetails } from "./storeDetails";
import { Geolocation } from "./location";
import { IAdDetails } from "./IAdDetails";

export class AdDetails {
    id: number;
    shortText: string;
    longText: string;
    category: string;
    adImageUrls: AdImageUrl[] = [];
    adPdf: AdPDF = new AdPDF();
    retailerId: number;
    retailerLogoUrl: string;
    retailerName: string;
    storeId: number;
    storeDetails: StoreDetails = new StoreDetails();
    distance: number;
    startDate: Date;
    createdBy: string;
    creationDate: Date;
    endDate: Date;
    logoPath: string;
    masterId: string;
    fileType: string;

    constructor(rawData?: IAdDetails) {
        if (rawData) {
            this.id = rawData.id ? rawData.id : 0;
            this.shortText = rawData.shortText ? rawData.shortText : "";
            this.longText = rawData.longText ? rawData.longText : "";
            this.category = rawData.category ? rawData.category : "";
            this.adImageUrls = rawData.adImageUrls ? rawData.adImageUrls : [];
            this.adPdf = rawData.adPdf ? rawData.adPdf : new AdPDF();
            this.retailerId = rawData.retailerId ? rawData.retailerId : 0;
            this.retailerLogoUrl = rawData.retailerLogoUrl ? rawData.retailerLogoUrl : "";
            this.retailerName = rawData.retailerName ? rawData.retailerName : "";
            this.storeId = rawData.storeId ? rawData.storeId : 0;
            this.storeDetails = rawData.storeDetails ? rawData.storeDetails : new StoreDetails();
            this.distance = rawData.distance ? rawData.distance : 0;
            this.startDate = rawData.startDate ? rawData.startDate : new Date();
            this.createdBy = rawData.createdBy ? rawData.createdBy : "";
            this.creationDate = rawData.creationDate ? rawData.creationDate : new Date();
            this.endDate = rawData.endDate ? rawData.endDate : new Date();
            this.logoPath = rawData.logoPath ? rawData.logoPath : "";
            this.masterId = rawData.masterId ? rawData.masterId : "";
            this.fileType = rawData.fileType ? rawData.fileType : "";
        }
        else {
            this.id = 0;
            this.category = "";
            this.adImageUrls = new AdImageUrl()[0];
            this.adPdf = new AdPDF();
            this.retailerId = 0;
            this.retailerLogoUrl = "";
            this.retailerName = "";
            this.storeId = 0;
            this.storeDetails = new StoreDetails();
            this.distance = 0;
            this.startDate = new Date();
            this.createdBy = "";
            this.creationDate = new Date();
            this.endDate = new Date();
            this.logoPath = "";
            this.masterId = "";
        }
    }

    public get allImageUrlsOrignials(): { url: string, pageNumber: number }[] {
        let images: { url: string, pageNumber: number }[] = [];
        if (this.adImageUrls !== undefined) {
            this.adImageUrls.forEach(img => {
                if (img.type === "original") {
                    let i: { url: string, pageNumber: number } = { url: "", pageNumber: 0 };
                    i.pageNumber = img.pdfPageNumber ? img.pdfPageNumber : 1;
                    if (images) {
                        i.url = img.imageUrl;
                    } else {
                        i.url = img.imageUrl;
                    }
                    images.push(i);
                }
            });
            return images;
        }
        else {
            return images;
        }
    }

    public get allImageUrlsOptimized1(): { url: string, pageNumber: number }[] {
        let images: { url: string, pageNumber: number }[] = [];
        if (this.adImageUrls !== undefined) {
            this.adImageUrls.forEach(img => {
                if (img.type === "optimized1") {
                    let i: { url: string, pageNumber: number } = { url: "", pageNumber: 0 };
                    i.pageNumber = img.pdfPageNumber ? img.pdfPageNumber : 1;
                    if (images) {
                        i.url = img.imageUrl;
                    } else {
                        i.url = img.imageUrl;
                    }
                    images.push(i);
                }
            });
            return images;
        }
        else {
            return images;
        }
    }

    public get allImageUrlsOptimized2(): { url: string, pageNumber: number }[] {
        let images: { url: string, pageNumber: number }[] = [];
        if (this.adImageUrls !== undefined) {
            this.adImageUrls.forEach(img => {
                if (img.type === "optimized2") {
                    let i: { url: string, pageNumber: number } = { url: "", pageNumber: 0 };
                    i.pageNumber = img.pdfPageNumber ? img.pdfPageNumber : 1;
                    if (images) {
                        i.url = img.imageUrl;
                    } else {
                        i.url = img.imageUrl;
                    }
                    images.push(i);
                }
            });
            return images;
        }
        else {
            return images;
        }
    }

    public get validityInDays(): number {
        var eventStartTime = new Date(this.startDate);
        var eventEndTime = new Date(this.endDate);
        var currentTime= new Date();
        var duration = eventEndTime.valueOf() - currentTime.valueOf();
        if(eventEndTime.valueOf() < currentTime.valueOf()){
            duration=0;
        }
        return Math.round((duration) / (1000 * 60 * 60 * 24));
    }

    public get coverimgUrlForMs(): string {
        let imgUrl: string;
        if (this.allImageUrlsOptimized2 !== undefined) {
            if (this.allImageUrlsOptimized1.findIndex(x => x.pageNumber === 1) >= 0) {
                imgUrl = this.allImageUrlsOptimized1.find(x => x.pageNumber === 1).url;
            }
            else {
                imgUrl = "";
            }
        }
        else {
            imgUrl = "";
        }
        return imgUrl;
    }

    public get coverimgUrlForXs(): string {
        let imgUrl: string;
        if (this.allImageUrlsOptimized2 !== undefined) {
            if (this.allImageUrlsOptimized2.findIndex(x => x.pageNumber === 1) >= 0) {
                imgUrl = this.allImageUrlsOptimized2.find(x => x.pageNumber === 1).url;
            }
            else {
                imgUrl = "";
            }
        }
        else {
            imgUrl = "";
        }
        return imgUrl;
    }

    public get coverimgUrlForLs(): string {
        let imgUrl: string;
        if (this.allImageUrlsOrignials !== undefined) {
            if (this.allImageUrlsOrignials.findIndex(x => x.pageNumber === 1) >= 0) {
                imgUrl = this.allImageUrlsOrignials.find(x => x.pageNumber === 1).url;
            }
            else {
                imgUrl = "";
            }
        }
        else {
            imgUrl = "";
        }
        return imgUrl;
    }

    public get status(): string {
        let s: string;
        let currentTime = new Date();
        if (new Date(this.endDate) > new Date()) {
            s = "Active";
        }
        else
        {
            s = "Inactive";
        }
        return s;
    }
}