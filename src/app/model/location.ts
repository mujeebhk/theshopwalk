export class Geolocation {
    public latitude: number;
    public longitude: number;

    constructor(){
        this.latitude = 0;
        this.longitude = 0;
    }
}