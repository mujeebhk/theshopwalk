import { Geolocation } from './location';
export interface IStoreDetails {
    address: string;
    landmark: string;
    latitude: number;
    longitude: number;
    phone: number;
    retailerId: number;
    storeName: string;
    storeTimings: string;
    id: number;
    creationDate: Date;
    modificationDate: Date;    
    city: string;
    location: Geolocation;
}