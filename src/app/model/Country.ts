import { ICountry } from "./ICountry";

export class Country {
    public name: string;
    public code: string;
    public creationDate: Date;
    public modificationDate: Date;

    constructor(rawData?: ICountry) {
        if (rawData) {
            this.code = rawData.code;
            this.creationDate = rawData.creationDate;
            this.modificationDate = rawData.modificationDate;
            this.name = rawData.name;
        }
        else {
            this.code = "";
            this.creationDate = new Date();
            this.modificationDate = new Date();
            this.name = "";
        }
    }
}