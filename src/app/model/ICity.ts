export interface ICity {
    name: string;
    code: string; 
    countyCode: string ;
    creationDate: Date ;
    modificationDate: Date ;
}