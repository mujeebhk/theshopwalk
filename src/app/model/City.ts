import { ICity } from "./ICity";

export class City {
    public name: string;
    public code: string;
    public countyCode: string;
    public creationDate: Date;
    public modificationDate: Date;

    constructor(rawData?: ICity) {
        if (rawData) {
            this.code = rawData.code;
            this.countyCode = rawData.countyCode;
            this.creationDate = rawData.creationDate;
            this.modificationDate = rawData.modificationDate;
            this.name = rawData.name;
        }
        else {
            this.code = "";
            this.countyCode = "";
            this.creationDate = new Date();
            this.modificationDate = new Date();
            this.name = "";
        }
    }
}