export class AdImageUrl {
    public imageId: string;
    public imageUrl: string;
    public type: string;
    public pdfPageNumber: number;
}