import { Component, OnInit, ViewChild } from '@angular/core';
import { IStoreDetails } from '../model/IStoreDetails';
import { Router, ActivatedRoute } from '@angular/router';
import { StoreService } from '../service/store.service';
import { RetailerService } from '../service/retailer.service';
import { RetailerDetails } from '../model/retailerDetails';
import { StoreCreateComponent } from '../store-create/store-create.component';
import { StoreUpdateComponent } from '../store-update/store-update.component';
import { StoreViewComponent } from '../store-view/store-view.component';
import { AdHomeComponent } from '../ad-home/ad-home.component';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { UrlHelper } from '../common/helpers/url.helper';
import { DynamicHelper } from '../common/helpers/dynamic.helper';

@Component({
  selector: 'app-store-home',
  templateUrl: './store-home.component.html',
  styleUrls: ['./store-home.component.scss']
})
export class StoreHomeComponent implements OnInit {

  public storeList: IStoreDetails[] = [];
  displayedColumns = ['name', 'locality', 'actions'];
  dataSource: IStoreDetails[];
  private retailerId: number;
  private retailerDetails: RetailerDetails = new RetailerDetails();
  isVisible: boolean = false;
  searchText: string;
  @ViewChild("storeCreate") public _storeCreate: StoreCreateComponent;
  @ViewChild("storeUpdate") public _storeUpdate: StoreUpdateComponent;
  @ViewChild("storeView") public _storeView: StoreViewComponent;
  @ViewChild("adhome") public _adhome: AdHomeComponent;

  constructor(private retailerService: RetailerService, private storeService: StoreService, private router: Router, private slimLoadingBarService: SlimLoadingBarService) {
    this.searchText = "";
  }

  ngOnInit() {
  }

  open(rId: number) {
    this.isVisible = true;
    this.retailerId = rId;
    this.getDatafromService();
  }

  private getDatafromService() {
    // logic to be verified
    this.slimLoadingBarService.start();
    this.retailerService.getRetailerById(this.retailerId).subscribe((data) => {
      this.retailerDetails = data;
    }, () => {
      this.storeService.getStoreListByRetailerId(this.retailerId).subscribe((data) => {
        this.storeList = data.sort(DynamicHelper.dynamicSort('storeName'));
        this.dataSource = this.storeList;
      }, () => {
        this.slimLoadingBarService.complete();
      }, () => {
        this.slimLoadingBarService.complete();
      });
    }, () => {
      this.storeService.getStoreListByRetailerId(this.retailerId).subscribe((data) => {
        this.storeList = data.sort(DynamicHelper.dynamicSort('storeName'));
        this.dataSource = this.storeList;
      }, () => {
        this.slimLoadingBarService.complete();
      }, () => {
        this.slimLoadingBarService.complete();
      });
    });
  }

  close() {
    this.isVisible = false;
    this.router.navigate(['retailers']);
  }

  showStoreHome(data: any) {
    this.retailerId = data["rid"];
    this.isVisible = true;
    this.getDatafromService();
  }

  OnCreateStore() {
    this.isVisible = false;
    this._storeCreate.open(this.retailerDetails);
  }

  OnUpdateStore(sId: number) {
    this.isVisible = false;
    this._storeUpdate.open(sId, this.retailerDetails.name);
  }

  OnViewStore(sId: number) {
    this.isVisible = false;
    this._storeView.open(sId, this.retailerDetails.name);
  }

  OnViewAds(sId: number) {
    this.isVisible = false;
    this._adhome.open(sId);
  }

  onSearch() {
    // angular pipe does all the dynamic filtering
  }

  onClear() {
    this.searchText = "";
  }
}
