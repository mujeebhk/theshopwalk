import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { IAdDetails } from '../model/IAdDetails';
import { RetailerService } from '../service/retailer.service';
import { StoreService } from '../service/store.service';
import { AdService } from '../service/ad.service';
import { RetailerDetails } from '../model/retailerDetails';
import { StoreDetails } from '../model/storeDetails';
import { AdViewComponent } from '../ad-view/ad-view.component';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AdDetails } from '../model/adDetails';
import { MatDialog } from '../../../node_modules/@angular/material';
import { AdChangeStatusDialogComponent } from '../ad-change-status-dialog/ad-change-status-dialog.component';
import { UrlHelper } from '../common/helpers/url.helper';
import { DynamicHelper } from '../common/helpers/dynamic.helper';

@Component({
  selector: 'app-ad-home',
  templateUrl: './ad-home.component.html',
  styleUrls: ['./ad-home.component.scss']
})
export class AdHomeComponent implements OnInit {
  public adList: IAdDetails[] = [];
  displayedColumns = ['shortText', 'locality', 'status', 'actions'];
  dataSource: IAdDetails[];
  private storeId: number;
  private retailerDetails: RetailerDetails = new RetailerDetails();
  private storeDetails: StoreDetails = new StoreDetails();
  public visible: boolean = false;
  searchText: string;

  @ViewChild("adView") private _adView: AdViewComponent;
  @ViewChild("adViewImages") private _adViewImages: AdViewComponent;

  @Output() public showStoreHome = new EventEmitter<any>();

  constructor(private retailerService: RetailerService, private storeService: StoreService, private adService: AdService, private slimLoadingBarService: SlimLoadingBarService, public dialog: MatDialog) {
    this.searchText = "";
  }

  ngOnInit() {
  }

  open(sId: number) {
    this.slimLoadingBarService.start();
    this.visible = true;
    this.storeId = sId;
    this.storeService.getStoreDetailsById(this.storeId).subscribe((data) => {
      this.storeDetails = data;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.retailerService.getRetailerById(this.storeDetails.retailerId).subscribe((data) => {
        this.retailerDetails = data;
      }, () => {
      }, () => {
      });
      this.adService.getAdsListByStoreId(this.storeId).subscribe((data) => {
        this.adList = data.sort(DynamicHelper.dynamicSort('shortText'));
        this.dataSource = this.adList;
      }, () => {
        this.slimLoadingBarService.complete();
      }, () => {
        this.slimLoadingBarService.complete();
      });
    });
  }

  OnViewDetails(aId: number) {
    this.visible = false;
    this._adView.open(aId);
  }

  OnViewImages(aId: number) {
    this.visible = false;
    this._adViewImages.open(aId);
  }

  showAdHome() {
    this.visible = true;
  }

  OnshowStoreHome() {
    this.visible = false;
    this.showStoreHome.emit({ rid: this.retailerDetails.id });
  }

  onSearch() {
    // angular pipe does all the dynamic filtering
  }

  onClear() {
    this.searchText = "";
  }

  OnChangeStatus(adDtls: AdDetails) {
    var updatedStatus: string;
    const dialogRef = this.dialog.open(AdChangeStatusDialogComponent, {
      width: '30%',
      data: { AdID: adDtls.id, Status: adDtls.status }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed : ' + result);
      if (result === "Updated") {
        this.slimLoadingBarService.start();
        this.adService.getAdsListByStoreId(this.storeId).subscribe((data) => {
          this.adList = data;
          this.dataSource = this.adList;
        }, () => {
          this.slimLoadingBarService.complete();
        }, () => {
          this.slimLoadingBarService.complete();
        });
      }
    });
  }
}
