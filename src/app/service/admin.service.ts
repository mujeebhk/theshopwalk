import { Injectable } from "../../../node_modules/@angular/core";
import { Http, RequestOptions } from '@angular/http';
import { UrlHelper } from "../common/helpers/url.helper";
import { AdminDetails } from "../model/adminDetails";
import { map } from "../../../node_modules/rxjs/operators";
import { promise } from "../../../node_modules/protractor";

@Injectable()
export class AdminService {
    private _baseUrl: string = "api/admin";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }

    login(data: AdminDetails) {
        let url = this._baseUrl + "/login";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    logOut() {
        let url = this._baseUrl + "/logout";
        return this.http.post(UrlHelper.buildUrl(url), "", this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    isLoggedIn(): Promise<any> {
        return new Promise((resolve, reject) => {
            let url = this._baseUrl + "/isLoggedIn";
            this.http.get(UrlHelper.buildUrl(url), this.options).subscribe((res) => {
                resolve(res.json());
            }, err => {
                reject(err);
            });
        });
    }
}