import { Injectable } from "../../../node_modules/@angular/core";
import { Http, RequestOptions } from "../../../node_modules/@angular/http";
import { Observable, Observer } from "../../../node_modules/rxjs";
import { UrlHelper } from "../common/helpers/url.helper";
import { map } from "../../../node_modules/rxjs/operators";

@Injectable()
export class GeoCoderService {
    private _baseUrl: string = "api/geoCoder";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }

    public getAddressDetailsByLatLong(data: any): Observable<number> {
        let url = this._baseUrl + "/getAddressDetailsByLatLng";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }
} 