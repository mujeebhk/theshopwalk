import { Http, RequestOptions } from '@angular/http';
import { Injectable } from "@angular/core";
import { StoreDetails } from '../model/storeDetails';
import { UrlHelper } from '../common/helpers/url.helper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class StoreService {
    private _baseUrl: string = "/api/store";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }


    public getStoreListByRetailerId(id: number): Observable<StoreDetails[]> {
        let url = this._baseUrl + "/getStoreList/" + id;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            let result: StoreDetails[] = [];
            for (let r of res.json()) {
                result.push(new StoreDetails(r));
            }
            return result;
        }));
    }

    public getStoreDetailsById(id: number): Observable<StoreDetails> {
        let url = this._baseUrl + "/getStore/" + id;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public createStore(data: any): Observable<number> {
        let url = this._baseUrl + "/create";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public updateStore(data: any): Observable<number> {
        let url = this._baseUrl + "/update";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public getStoreCount(): Observable<number> {
        let url = this._baseUrl + "/count";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }
}