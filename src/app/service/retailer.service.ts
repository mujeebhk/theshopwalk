import { Http, RequestOptions } from '@angular/http';
import { Injectable } from "@angular/core";
import { promise } from 'protractor';
import { RetailerDetails } from '../model/retailerDetails';
import { UrlHelper } from '../common/helpers/url.helper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
// import { map } from 'rxjs/operators';
// import { Observable } from 'rxjs'; 

@Injectable()
export class RetailerService {
    private _baseUrl: string = "/api/retailer";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }

    public getRetailerList(): Observable<RetailerDetails[]> {
        let url = this._baseUrl + "/getRetailersList";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            let result: RetailerDetails[] = [];
            for (let r of res.json()) {
                result.push(new RetailerDetails(r));
            }
            return result;
        }));
    }

    public getRetailerById(id: number): Observable<RetailerDetails> {
        let url = this._baseUrl + "/getRetailer/" + id;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public createRetailer(data: any): Observable<number> {
        let url = this._baseUrl + "/create";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public updateRetailer(data: any): Observable<number> {
        let url = this._baseUrl + "/update";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public getRetailerCount(): Observable<number> {
        let url = this._baseUrl + "/count";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public ValidateRetailerNameExists(name: String) {
        let url = this._baseUrl + "/retailerNameExists/" + name;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }
}