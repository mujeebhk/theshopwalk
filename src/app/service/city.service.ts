import { Injectable } from "@angular/core";
import { Http, RequestOptions } from '@angular/http';
import { City } from "../model/City";
import { Observable } from "../../../node_modules/rxjs";
import { UrlHelper } from "../common/helpers/url.helper";
import { map } from "../../../node_modules/rxjs/operators";

@Injectable()
export class CityService {
    private _baseUrl: string = "api/city";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }

    public getCities(): Observable<City[]> {
        let url = this._baseUrl + "/getCities";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map(res => {
            let result: City[] = [];
            for (let r of res.json()) {
                result.push(new City(r));
            }
            return result;
        }))
    }
}