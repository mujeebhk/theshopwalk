import { Http, RequestOptions } from '@angular/http';
import { Injectable } from "@angular/core";
import { AdDetails } from '../model/adDetails';
import { UrlHelper } from '../common/helpers/url.helper';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class AdService {
    private _baseUrl: string = "api/ads";
    public headers: any;
    public options = new RequestOptions({
        headers: this.headers,
        withCredentials: true
      });

    constructor(private http: Http) {
        this.headers = new Headers( {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
    }

    public getAdsListByLocation(address: string): Observable<AdDetails[]> {
        let url = this._baseUrl + "/getAdsListByLocation";
        return this.http.post(UrlHelper.buildUrl(url), { "address": address }, this.options).pipe(map(res => {
            let result: AdDetails[] = [];
            for (let r of res.json()) {
                result.push(new AdDetails(r));
            }
            return result;
        }))
    }

    public getAdsDetails(address: string): Observable<AdDetails[]> {
        let data = { "address": address };
        let url = this._baseUrl + "/getAdsListByLocation";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map(res => {
            let result: AdDetails[] = [];
            for (let r of res.json()) {
                result.push(new AdDetails(r));
            }
            return result;
        }))
    }

    public getAdDetailsById(id: number): Observable<AdDetails> {
        let url = this._baseUrl + "/getAd/" + id;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map(res => {
            return res.json();
        }))
    }

    public getAdsListByStoreId(id: number): Observable<AdDetails[]> {
        let url = this._baseUrl + "/getStoreAdList/" + id;
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            let result: AdDetails[] = [];
            for (let r of res.json()) {
                result.push(new AdDetails(r));
            }
            return result;
        }));
    }

    public createAd(data: any): Observable<number> {
        let url = this._baseUrl + "/create";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public updateAd(data: AdDetails): Observable<number> {
        let url = this._baseUrl + "/update";
        return this.http.post(UrlHelper.buildUrl(url), data, this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public getAdList(): Observable<AdDetails[]> {
        let url = this._baseUrl + "/getAdsList";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            let result: AdDetails[] = [];
            for (let r of res.json()) {
                result.push(new AdDetails(r));
            }
            return result;
        }));
    }

    public getAdCount(): Observable<number> {
        let url = this._baseUrl + "/count";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }

    public getActiveAdsCount(): Observable<number> {
        let url = this._baseUrl + "/active/count";
        return this.http.get(UrlHelper.buildUrl(url), this.options).pipe(map((res) => {
            return res.json();
        }));
    }
}