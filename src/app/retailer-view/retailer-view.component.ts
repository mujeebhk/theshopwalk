import { Component, OnInit } from '@angular/core';
import { RetailerDetails } from '../model/retailerDetails';
import { RetailerService } from '../service/retailer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { CountryService } from '../service/country.service';
import { Country } from '../model/Country';

@Component({
  selector: 'app-retailer-view',
  templateUrl: './retailer-view.component.html',
  styleUrls: ['./retailer-view.component.scss']
})
export class RetailerViewComponent implements OnInit {
  public retailerData: RetailerDetails = new RetailerDetails();
  public isSuccess: boolean = false;
  public retailerId: number;
  public isVisible: boolean = false;
  selectedCountry: string = "UAE";
  countries: Country[] = [];

  constructor(private retailerService: RetailerService, private countryService: CountryService, private router: Router, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.countryService.getCities().subscribe((data) => {
      this.countries = data;
    });
  }

  ngOnInit() {
  }

  public open(rId: number) {
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.retailerId = rId;
    this.retailerService.getRetailerById(this.retailerId).subscribe((data) => {
      this.retailerData = data;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    });
  }

  public OnClose() {
    this.isVisible = false;
    this.router.navigate(['retailers']);
  }
}