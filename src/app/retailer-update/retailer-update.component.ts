import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RetailerService } from '../service/retailer.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RetailerDetails } from '../model/retailerDetails';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { CountryService } from '../service/country.service';
import { Country } from '../model/Country';

@Component({
  selector: 'app-retailer-update',
  templateUrl: './retailer-update.component.html',
  styleUrls: ['./retailer-update.component.scss']
})
export class RetailerUpdateComponent implements OnInit {
  public retailerData: RetailerDetails = new RetailerDetails();
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  isVisible: boolean = false;
  retailerId: number;
  imageName: string;
  selectedCountry: string;
  countries: Country[] = [];
  files: any;
  isValidImgSize: boolean = true;

  constructor(private retailerService: RetailerService, private countryService: CountryService, private router: Router, private route: ActivatedRoute, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.countryService.getCities().subscribe((data) => {
      this.countries = data;
    });
  }

  ngOnInit() {
  }

  public OnSave() {
    this.slimLoadingBarService.start();
    console.log(this.retailerData);
    this.retailerService.updateRetailer(this.retailerData).subscribe((data) => {
      this.isSuccess = true;
      this.showMsg = true;
      this.slimLoadingBarService.complete();
      setTimeout(() => {
        this.router.navigate(['retailers']);
      }, 2000)
    }, (e) => {
      this.slimLoadingBarService.complete();
      this.isSuccess = false;
      this.showMsg = true;
    });
  }

  public OnCancel() {
    this.clear();
    this.isVisible = false;
    this.router.navigate(['retailers']);
  }

  public clear() {
    this.retailerData = new RetailerDetails();
  }

  onOpen(rId: number) {
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.retailerId = rId;
    this.retailerService.getRetailerById(this.retailerId).subscribe((data) => {
      this.retailerData = data;
      this.selectedCountry = data.country;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    });
  }

  getFiles(event) {
    this.isValidImgSize = true;
    this.files = event.target.files;
    var fileSize = this.files[0].size / 1024 / 1024
    if (fileSize <= 1) {
      this.isValidImgSize = true;
      // this._handleReaderLoaded.bind(this);
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.files[0]);
      // console.log(reader);
    }
    else {
      this.isValidImgSize = false;
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.retailerData.logopath = "data:image;base64," + btoa(binaryString);  // Converting binary string data.
    // console.log(this.retailerData);
  }
}