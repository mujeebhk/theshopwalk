import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StoreDetails } from '../model/storeDetails';
import { StoreService } from '../service/store.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { City } from '../model/City';
import { CityService } from '../service/city.service';

@Component({
  selector: 'app-store-view',
  templateUrl: './store-view.component.html',
  styleUrls: ['./store-view.component.scss']
})
export class StoreViewComponent implements OnInit {
  public storeDetails: StoreDetails = new StoreDetails();
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  public retailerId: number;
  public storeId: number;
  public navigateUrl: string;
  public isVisible: boolean = false;
  public retailerName: string;
  selectedCity: string;
  // cities: { name: string, code: string }[] = [{ name: "city1", code: "city1"},{ name: "city2", code: "city2"},{ name: "city3", code: "city3"}, ];
  cities: City[] = [];

  @Output() public showStoreHome = new EventEmitter<any>();

  constructor(private storeService: StoreService, private cityService: CityService, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.cityService.getCities().subscribe((data) => {
      this.cities = data;
    });
  }

  ngOnInit() {
  }

  open(sId: number, retailerName: string) {
    this.retailerName = retailerName;
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.storeId = sId;
    this.storeService.getStoreDetailsById(this.storeId).subscribe((data) => {
      this.storeDetails = data;
      this.selectedCity = data.city;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    }
    );
  }

  public OnClose() {
    this.isVisible = false;
    this.showStoreHome.emit({ rid: this.storeDetails.retailerId });
    this.clear();
  }

  public clear() {
    this.storeDetails = new StoreDetails();
  }
}

