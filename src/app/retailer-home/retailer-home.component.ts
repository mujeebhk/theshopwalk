import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RetailerService } from '../service/retailer.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { IRetailerDetails } from '../model/IretailerDetails';
import { Router } from '@angular/router';
import { RetailerCreateComponent } from '../retailer-create/retailer-create.component';
import { RetailerUpdateComponent } from '../retailer-update/retailer-update.component';
import { RetailerViewComponent } from '../retailer-view/retailer-view.component';
import { StoreHomeComponent } from '../store-home/store-home.component';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { UrlHelper } from '../common/helpers/url.helper';
import { DynamicHelper } from '../common/helpers/dynamic.helper';

@Component({
  selector: 'app-retailer-home',
  templateUrl: './retailer-home.component.html',
  styleUrls: ['./retailer-home.component.scss']
})
export class RetailerHomeComponent implements OnInit {
  public retailerList: IRetailerDetails[] = [];
  public displayedColumns = ['name', 'id', 'phone', 'email'];
  public dataSource: IRetailerDetails[];
  public visible: boolean = true;
  public searchText: string;

  @ViewChild("retailerCreate") private _retailerCreate: RetailerCreateComponent;
  @ViewChild("retailerUpdate") private _retailerUpdate: RetailerUpdateComponent;
  @ViewChild("retailerView") private _retailerView: RetailerViewComponent;
  @ViewChild("storehome") private _storehome: StoreHomeComponent;

  constructor(private retailerService: RetailerService, private slimLoadingBarService: SlimLoadingBarService) {
    this.slimLoadingBarService.start();
    this.searchText = "";
    this.retailerService.getRetailerList().subscribe((data) => {
      this.retailerList = data.sort(DynamicHelper.dynamicSort('name'));
      this.dataSource = this.retailerList;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    });
  }

  ngOnInit() {
  }

  OnCreateRetailer() {
    this.visible = false;
    this._retailerCreate.onOpen();
  }

  OnUpdateRetailer(id: number) {
    this.visible = false;
    this._retailerUpdate.onOpen(id);
  }

  OnViewRetailer(id: number) {
    this.visible = false;
    this._retailerView.open(id);
  }

  OnViewStores(id: number) {
    this.visible = false;
    this._storehome.open(id);
  }

  onSearch() {
    // angular pipe does all the dynamic filtering
  }

  onClear() {
    this.searchText = "";
  }
}