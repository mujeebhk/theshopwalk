import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../node_modules/@angular/material';
import { SlimLoadingBarService } from '../../../node_modules/ng2-slim-loading-bar';
import { Address } from '../../../node_modules/ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from '../../../node_modules/ngx-google-places-autocomplete';
import { LatLng } from '../../../node_modules/ngx-google-places-autocomplete/objects/latLng';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-location-change-dialog',
  templateUrl: './location-change-dialog.component.html',
  styleUrls: ['./location-change-dialog.component.scss']
})
export class LocationChangeDialogComponent {
  currentLocation: string;
  isUpdated: boolean = true;
  selectedLocation: { name: string, formattedAddress: string, location: any } = { name: "", formattedAddress: "", location: "" };
  @ViewChild('places') places: GooglePlaceDirective;
  options = {
    types: [],
    componentRestrictions: { country: environment.googleLocationCountry }
  };
  isSuccessful: boolean = true;
  constructor(private slimLoadingBarService: SlimLoadingBarService,
    public dialogRef: MatDialogRef<LocationChangeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { currentLocation: string }) {
    console.log(this.options);
    this.currentLocation = data.currentLocation;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onUpdateClick() {
    this.dialogRef.close(this.selectedLocation);
  }

  onClearClick() {
    this.dialogRef.close("clear");
  }

  public handleAddressChange(address: Address) {
    console.log(address);
    this.isUpdated = false;
    this.selectedLocation.name = address.name;
    this.selectedLocation.formattedAddress = address.formatted_address;
    this.selectedLocation.location = address.geometry.location.toJSON();
  }
}
