import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationChangeDialogComponent } from './location-change-dialog.component';

describe('LocationChangeDialogComponent', () => {
  let component: LocationChangeDialogComponent;
  let fixture: ComponentFixture<LocationChangeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationChangeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationChangeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
