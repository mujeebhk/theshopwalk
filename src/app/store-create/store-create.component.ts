import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StoreService } from '../service/store.service';
import { Router, ActivatedRoute } from '@angular/router';
import { StoreDetails } from '../model/storeDetails';
import { RetailerDetails } from '../model/retailerDetails';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { CityService } from '../service/city.service';
import { City } from '../model/City';

@Component({
  selector: 'app-store-create',
  templateUrl: './store-create.component.html',
  styleUrls: ['./store-create.component.scss']
})
export class StoreCreateComponent implements OnInit {
  public storeDetails: { storeName: string, retailerId: number, latitude: number, longitude: number, address: string, landmark: string, phone: number, city: string, storeTimings: string }
    = { storeName: "", retailerId: 0, latitude: null, longitude: null, address: "", landmark: "", phone: null, city: "", storeTimings: "" };
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  public retailerId: number;
  public navigateUrl: string;
  public isVisible: boolean = false;
  retailerDetails: RetailerDetails = new RetailerDetails();
  selectedCity: string = "";
  // cities: { name: string, code: string }[] = [{ name: "city1", code: "city1" }, { name: "city2", code: "city2" }, { name: "city3", code: "city3" },];
  cities: City[] = [];
  @Output() public showStoreHome = new EventEmitter<any>();

  constructor(private storeService: StoreService,private cityService: CityService, private router: Router, private route: ActivatedRoute, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.cityService.getCities().subscribe((data) => {
      this.cities = data;
    });
  }

  ngOnInit() {
  }

  open(retailerDetails: RetailerDetails) {
    this.isVisible = true;
    this.retailerDetails = retailerDetails;
    this.storeDetails.retailerId = this.retailerDetails.id;
  }

  public OnSave() {
    this.slimLoadingBarService.start();
    this.storeService.createStore(this.storeDetails).subscribe((data) => {
      this.isSuccess = true;
      this.showMsg = true;
      this.slimLoadingBarService.complete();
      setTimeout(() => {
        this.isVisible = false;
        this.showMsg = false;
        this.showStoreHome.emit({ rid: this.retailerDetails.id });
        this.clear();
      }, 2000)
    }, () => {
      this.isSuccess = false;
      this.showMsg = true;
    });
  }

  public OnCancel() {
    this.isVisible = false;
    this.showStoreHome.emit({ rid: this.retailerDetails.id });
    this.clear();
  }

  public clear() {
    this.storeDetails = { storeName: "", retailerId: 0, latitude: null, longitude: null, address: "", landmark: "", phone: null, city: "", storeTimings: "" };
  }
}
