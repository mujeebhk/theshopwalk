import { Component, OnInit, ViewChild } from '@angular/core';
import { AdDetailsComponent } from '../ad-details/ad-details.component'
import { AdService } from '../service/ad.service';
import { IAdDetails } from '../model/IAdDetails';
import { error } from '@angular/compiler/src/util';
import { AdDetails } from '../model/adDetails';
import { MatDialog } from '../../../node_modules/@angular/material';
import { LocationChangeDialogComponent } from '../location-change-dialog/location-change-dialog.component';
import { Address } from '../../../node_modules/ngx-google-places-autocomplete/objects/address';
import { SlimLoadingBarService } from '../../../node_modules/ng2-slim-loading-bar';
import { DynamicHelper } from '../common/helpers/dynamic.helper';
import { HostListener } from "@angular/core";
import { environment } from '../../environments/environment';
import { GeoCoderService } from '../service/geoCoder.service';

@Component({
  selector: 'app-main-gallery',
  templateUrl: './main-gallery.component.html',
  styleUrls: ['./main-gallery.component.scss']
})
export class MainGalleryComponent implements OnInit {
  
  isSmallScreen: boolean = false;
  isMediumScreen: boolean = false;
  isLargeScreen: boolean = false;
  isAdAvailable = false;

  screenHeight: number;
  screenWidth: number;
  private images: { title, path, validFor, Offer }[] = [];
  private adList: IAdDetails[] = [];
  currentLocation: { name: string, formattedAddress: string, location: any } = { name: "", formattedAddress: "", location: "" };
  address: string = "No location selected";
  shortAddress: string = "";
  isVisible: boolean = true;
  isFetchingLocation: boolean = true;
  @ViewChild("adDetails") private _adDetails: AdDetailsComponent;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.isSmallScreen = false;
    this.isMediumScreen = false;
    this.isLargeScreen = false;

    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    if (this.screenWidth <= environment.smallScreenWidth) {
      this.isSmallScreen = true;
    }
    else if (this.screenWidth > environment.MediumScreenWidthMin && this.screenWidth <= environment.MediumScreenWidthMax) {
      this.isMediumScreen = true;
    }
    else if (this.screenWidth >= environment.LargeScreenWidth) {
      this.isLargeScreen = true;
    }

    console.log("------");
    console.log(this.screenWidth);
    console.log(this.isSmallScreen);
    console.log(this.isMediumScreen);
    console.log(this.isLargeScreen);
    console.log("------");
  }
  

  constructor(private adService: AdService, public geoCoderService: GeoCoderService, public dialog: MatDialog, private slimLoadingBarService: SlimLoadingBarService) {
    this.onResize();
    this.address = localStorage.getItem("address");
    console.log(this.address);
    console.log("calling loadAdsBasedOnAddress");
    this.loadAdsBasedOnAddress();
  }


  private loadAdsBasedOnAddress() {
    if (this.address == null || this.address === "No location selected") {
      this.slimLoadingBarService.start();
      // check for default value
      if (environment.defaultLatitude && environment.defaultLongitude) {
        console.log("calling getAddressDetailsByLatLong from default latlng : " + environment.defaultLatitude + " : " + environment.defaultLongitude);
        this.geoCoderService.getAddressDetailsByLatLong({ lat: environment.defaultLatitude, lng: environment.defaultLongitude }).subscribe((data: any) => {
          if (data) {
            this.address = data.formattedAddress;
            this.shortAddress = this.address ? this.address.split(',')[0] : "No location selected";
            console.log("calling getAddressDetailsByLatLong : " + this.address);
            this.adService.getAdsListByLocation(this.address).subscribe((data: AdDetails[]) => {
              this.isFetchingLocation =false;
              if (data.length === 0) {
                 this.isAdAvailable = true;
              } else {
                this.isAdAvailable = false;
              }
              this.adList = data.sort(DynamicHelper.dynamicSort('distance', "number"));
            }, e => {
              console.log('Error: ', e);
            }, () => {
              this.slimLoadingBarService.complete();
            });
          }
          else {
            this.isFetchingLocation =false;
            this.address = "No location selected";
            this.shortAddress = "No location selected";
            console.log("calling getAddressDetailsByLatLong : getAdList");
            this.adService.getAdList().subscribe((data: AdDetails[]) => {
              if (data.length === 0) {
                this.isAdAvailable = true;
             } else {
               this.isAdAvailable = false;
             }
              this.adList = data;
            }, e => {
              console.log('Error: ', e);
            }, () => {
              this.slimLoadingBarService.complete();
            });
          }
        });
      }
    }
    else {
      this.isFetchingLocation =false;
      this.slimLoadingBarService.start();
      this.shortAddress = localStorage.getItem("shortAddress");
      console.log("calling getAddressDetailsByLatLong : else part : getAdsListByLocation : " + this.address);
      this.adService.getAdsListByLocation(this.address).subscribe((data: AdDetails[]) => {
        if (data.length === 0) {
          this.isAdAvailable = true;
       } else {
         this.isAdAvailable = false;
       }
        this.adList = data.sort(DynamicHelper.dynamicSort('distance', 'number'));
      }, e => {
        console.log('Error: ', e);
      }, () => {
        this.slimLoadingBarService.complete();
      });
    }
  }

  ngOnInit() {
  }

  OnLocationClick() {
    let width: string;
    if (this.screenWidth <= environment.smallScreenWidth) {
      width = "50%";
    }
    else if (this.screenWidth > environment.MediumScreenWidthMin && this.screenWidth <= environment.MediumScreenWidthMax) {
      width = "40%";
    }
    else if (this.screenWidth >= environment.LargeScreenWidth) {
      width = "30%";
    }
    const dialogRef = this.dialog.open(LocationChangeDialogComponent, {
      width: width,
      data: { currentLocation: this.address ? this.address : "N/A" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        // no change in the location , so no need to call api
      }
      else if (result === "clear") {
        // this.slimLoadingBarService.start();
        this.currentLocation = { name: "", formattedAddress: "", location: "" };
        this.address = "No location selected";
        this.shortAddress = "";
        // remove address from local storage
        localStorage.removeItem("address");
        localStorage.removeItem("shortAddress");
        this.loadAdsBasedOnAddress();
      }
      else {
        this.slimLoadingBarService.start();
        this.currentLocation = result;
        this.address = this.currentLocation ? this.currentLocation.formattedAddress : "N/A";
        this.shortAddress = this.currentLocation ? this.currentLocation.name : "N/A";
        // add address in local storage
        localStorage.setItem("address", this.address);
        localStorage.setItem("shortAddress", this.shortAddress);
        this.adService.getAdsListByLocation(this.address).subscribe((data: AdDetails[]) => {

          if (data.length === 0) {
            this.isAdAvailable = true;
         } else {
           this.isAdAvailable = false;
         }
          this.adList = data.sort(DynamicHelper.dynamicSort('distance', "number"));
          console.log(this.adList);
        }, e => {
          console.log('Error: ', e);
        }, () => {
          this.slimLoadingBarService.complete();
        });
      }
    });
  }

  openDetails(adId: number) {
    this.isVisible = false;
    this._adDetails.open(adId);
  }

  openHome() {
    this.isVisible = true;
  }
}
