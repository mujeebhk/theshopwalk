import { Component, OnInit, ViewChild } from '@angular/core';
import { IAdDetails } from '../model/IAdDetails';
import { AdService } from '../service/ad.service';
import { AdCreateComponent } from '../ad-create/ad-create.component';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { UrlHelper } from '../common/helpers/url.helper';
import { DynamicHelper } from '../common/helpers/dynamic.helper';

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.scss']
})
export class AdListComponent implements OnInit {

  public adList: IAdDetails[] = [];
  displayedColumns = ['shortText', 'longText', 'Rname', 'Sname', 'locality'];
  dataSource: IAdDetails[];
  visible: boolean = true;
  searchText: string;

  @ViewChild("adCreate") private _adCreate: AdCreateComponent;

  constructor(private adService: AdService, private slimLoadingBarService: SlimLoadingBarService) {
    this.slimLoadingBarService.start();
    this.searchText = "";
    this.adService.getAdList().subscribe((data) => {
      this.adList = data;
      this.dataSource = this.adList;
      console.log(data);
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    }
    );
  }

  ngOnInit() {
  }

  OnCreateAd() {
    this.visible = false;
    this._adCreate.open();
  }
  onSearch() {
    // angular pipe does all the dynamic filtering
  }

  onClear() {
    this.searchText = "";
  }
}
