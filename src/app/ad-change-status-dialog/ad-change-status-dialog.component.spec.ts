import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdChangeStatusDialogComponent } from './ad-change-status-dialog.component';

describe('AdChangeStatusDialogComponent', () => {
  let component: AdChangeStatusDialogComponent;
  let fixture: ComponentFixture<AdChangeStatusDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdChangeStatusDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdChangeStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
