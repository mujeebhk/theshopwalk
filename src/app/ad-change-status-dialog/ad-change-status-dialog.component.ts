import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../node_modules/@angular/material';
import { AdService } from '../service/ad.service';
import { AdDetails } from '../model/adDetails';
import { SlimLoadingBarService } from '../../../node_modules/ng2-slim-loading-bar';

@Component({
  selector: 'app-ad-change-status-dialog',
  templateUrl: './ad-change-status-dialog.component.html',
  styleUrls: ['./ad-change-status-dialog.component.scss']
})
export class AdChangeStatusDialogComponent {
  selectedStatus: string;
  StatusList: string[] = ['active', 'inactive', 'deleted'];
  AdId: number;
  isSuccessful: boolean = true;

  constructor(private adService: AdService, private slimLoadingBarService: SlimLoadingBarService,
    public dialogRef: MatDialogRef<AdChangeStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { AdID: number, Status: string }) {
    this.AdId = data.AdID;
    if (data.Status) {
      this.selectedStatus = data.Status;
    }
    else {
      this.selectedStatus = this.StatusList[0];
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onUpdateClick() {
    if (this.data.Status !== this.selectedStatus) {
      this.slimLoadingBarService.start();
      // update in db
      var ad: AdDetails = new AdDetails();
      this.adService.getAdDetailsById(this.data.AdID).subscribe((data) => {
        // refill missing data as we dont get properties if they dont have values against them in db
        ad = new AdDetails(data);
        // ad.status = this.selectedStatus;
        this.adService.updateAd(ad).subscribe(() => {
          this.slimLoadingBarService.complete();
          this.dialogRef.close("Updated");
        }, () => {
          this.slimLoadingBarService.complete();
          this.isSuccessful = false;
        });
      })
    }
  }
}
