import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AdDetails } from '../model/adDetails';
import { RetailerDetails } from '../model/retailerDetails';
import { StoreDetails } from '../model/storeDetails';
import { AdService } from '../service/ad.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss']
})
export class AdViewComponent implements OnInit {
  public retailerDetails: RetailerDetails = new RetailerDetails();
  public storeDetails: StoreDetails = new StoreDetails();
  public adDetails: AdDetails = new AdDetails();
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  public retailerId: number;
  public storeId: number;
  public adId: number;
  public navigateUrl: string;
  isVisible: boolean = false;
  sDate: any;
  eDate: any;
  originalImgs: string;

  @Output() public showAdHome = new EventEmitter<any>();

  constructor(private adService: AdService, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
  }

  ngOnInit() {
  }

  open(aId: number) {
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.adId = aId;
    this.adService.getAdDetailsById(this.adId).subscribe((data) => {
      this.adDetails = new AdDetails(data);
      console.log(this.adDetails);
      this.sDate = new Date(data.startDate).toLocaleDateString();
      this.eDate = new Date(data.endDate).toLocaleDateString();
      this.originalImgs = null;
      if (this.adDetails.allImageUrlsOrignials) {
        this.adDetails.allImageUrlsOrignials.forEach((data) => {
          this.originalImgs = this.originalImgs ? this.originalImgs + ';' + data.url : data.url;
        });
      }
      console.log(this.originalImgs);
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    });
  }

  public OnClose() {
    this.clear();
    this.isVisible = false;
    this.showAdHome.emit({ rid: this.adDetails.retailerId });
  }

  public clear() {
    this.adDetails = new AdDetails();
  }
}
