import { Component, OnInit } from '@angular/core';
import { RetailerService } from '../service/retailer.service';
import { AdService } from '../service/ad.service';
import { StoreService } from '../service/store.service';
import { RetailerDetails } from '../model/retailerDetails';
import { AdDetails } from '../model/adDetails';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

@Component({
  selector: 'app-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.scss']
})
export class DashboardDetailsComponent implements OnInit {

  visible: boolean = false;
  adCount: number = 0;
  retailerCount: number = 0;
  storeCount: number = 0;
  activeAdCount: number = 0;

  constructor(private retailerService: RetailerService, private storeService: StoreService, private adService: AdService, private slimLoadingBarService: SlimLoadingBarService) {
    this.retailerService.getRetailerCount().subscribe((data: number) => {
      this.slimLoadingBarService.start();
      this.retailerCount = data;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.storeService.getStoreCount().subscribe((data: number) => {
        this.slimLoadingBarService.start();
        this.storeCount = data;
      }, () => {
        this.slimLoadingBarService.complete();
      }, () => {
        this.adService.getAdCount().subscribe((data: number) => {
          this.slimLoadingBarService.start();
          this.adCount = data;
        }, () => {
          this.slimLoadingBarService.complete();
        }, () => {
          this.adService.getActiveAdsCount().subscribe((data: number) => {
            this.activeAdCount = data;
          });
          this.slimLoadingBarService.complete();
        });
      });
    });

    // this.storeService.getstoreList().subscribe((data : RetailerDetails[]) => {
    //   this.retailerCount = data.length;
    // });


  }

  ngOnInit() {
  }
}
