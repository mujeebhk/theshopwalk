import { Component, OnInit } from '@angular/core';
import { RetailerService } from '../service/retailer.service';
import { Router } from '@angular/router';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Country } from '../model/Country';
import { CountryService } from '../service/country.service';

@Component({
  selector: 'app-retailer-create',
  templateUrl: './retailer-create.component.html',
  styleUrls: ['./retailer-create.component.scss']
})
export class RetailerCreateComponent implements OnInit {

  public retailerData: { name: string, phone: string, email: string, websiteUrl: string, logopath: string, country: string }
    = { name: "", email: "", logopath: null, websiteUrl: "", phone: "", country: "UAE"};
  public isSuccess: boolean = false;
  public showMsg: boolean = false;
  isVisible: boolean = false;
  files: any;
  isValidImgSize: boolean = true;
  imageName: string;
  isInvalid: boolean = false;
  selectedCountry: string;
  // countries: {name: string, code: string}[] = [{name: "UAE", code: "UAE"}];
  countries: Country[] = [];
  duplicateName: string;
 
  constructor(private retailerService: RetailerService,
    private countryService: CountryService, private router: Router, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.isValidImgSize = true;
    this.selectedCountry = "UAE";
    this.countryService.getCities().subscribe((data) => {
      this.countries = data;
    });
  }

  ngOnInit() {
  }

  public ValidateRetailerNameForuniqueness(event){
    // console.log("calling");
    console.log(event);
    if (this.retailerData.name){
      this.duplicateName = this.retailerData.name;
      this.retailerService.ValidateRetailerNameExists(this.retailerData.name).subscribe((data) => {
        this.isInvalid = data;
        if (this.isInvalid){
          this.retailerData.name = "";
        }
      });
    }
    else
    {
      this.isInvalid = false;
    }
  }

  public OnSave() {
    this.retailerService.ValidateRetailerNameExists(this.retailerData.name).subscribe((data) => {
      this.isInvalid = data;
      if (!data) {
      this.slimLoadingBarService.start();
      this.retailerService.createRetailer(this.retailerData).subscribe(() => {
        this.isSuccess = true;
        this.showMsg = true;
        this.slimLoadingBarService.complete();
        setTimeout(() => {
          this.router.navigate(['retailers']);
        }, 2000);
      }, () => {
        this.isSuccess = false;
        this.showMsg = true;
      });
    }
    });
  }

  public OnCancel() {
    this.clear();
    this.isVisible = false;
    this.router.navigate(['retailers']);
  }

  public clear() {
    this.retailerData = { name: "", email: "", logopath: null, websiteUrl: "", phone: "", country: "UAE" };
  }

  onOpen() {
    this.isVisible = true;
  }

  close() {
    this.isVisible = false;
  }

  getFiles(event) {
    this.isValidImgSize = true;
    this.files = event.target.files;
    var fileSize = this.files[0].size / 1024 / 1024
    if (fileSize <= 1) {
      this.isValidImgSize = true;
      // this._handleReaderLoaded.bind(this);
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(this.files[0]);
      // console.log(reader);
    }
    else {
      this.isValidImgSize = false;
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.retailerData.logopath = "data:image;base64," + btoa(binaryString);  // Converting binary string data.
    // console.log(this.retailerData.logopath);
  }

}