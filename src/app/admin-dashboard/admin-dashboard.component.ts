import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../service/admin.service';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  id: number;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, private route: ActivatedRoute, private adminService: AdminService) {
    if (this.router.url.includes("dashboard")) {
      this.id = 1;
    }
    else if (this.router.url.includes("retailers")) {
      this.id = 2;
    }
    else if (this.router.url.includes("advert")) {
      this.id = 3;
    }
  }

  ngOnInit() {
  }

  openDashboardDetails() {
    // this.router.navigate(['/'], {skipLocationChange: true})
    //     .then(() => { this.router.navigate(['/dashboard']); });
    this.router.navigate(['/dashboard']);
  }

  openRetailerHome() {
    this.router.navigate(['/retailers']);
  }

  openAdvertHome() {
    this.router.navigate(['/adverts']);
  }

  logout() {
    this.adminService.logOut().subscribe(() => {
      this.router.navigate(['/tswlogin']);
    });
  }
}
