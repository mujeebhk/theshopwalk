import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StoreDetails } from '../model/storeDetails';
import { StoreService } from '../service/store.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RetailerDetails } from '../model/retailerDetails';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { CityService } from '../service/city.service';
import { City } from '../model/City';

@Component({
  selector: 'app-store-update',
  templateUrl: './store-update.component.html',
  styleUrls: ['./store-update.component.scss']
})
export class StoreUpdateComponent implements OnInit {
  storeDetails: StoreDetails = new StoreDetails();
  retailerDetails: RetailerDetails = new RetailerDetails();
  isSuccess: boolean = false;
  showMsg: boolean = false;
  storeId: number;
  navigateUrl: string;
  isVisible: boolean = false;
  retailerName: string;
  selectedCity: string;
  // cities: { name: string, code: string }[] = [{ name: "city1", code: "city1" }, { name: "city2", code: "city2" }, { name: "city3", code: "city3" },];
  cities: City[] = [];

  @Output() public showStoreHome = new EventEmitter<any>();

  constructor(private storeService: StoreService, private cityService: CityService, private slimLoadingBarService: SlimLoadingBarService) {
    this.isSuccess = true;
    this.showMsg = false;
    this.cityService.getCities().subscribe((data) => {
      this.cities = data;
    });
  }

  ngOnInit() {
  }

  open(sid: number, retailerName: string) {
    this.retailerName = retailerName;
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.storeId = sid;
    this.storeService.getStoreDetailsById(this.storeId).subscribe((data) => {
      this.storeDetails = data;
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    }
    );
  }

  public OnSave() {
    this.storeDetails.latitude = this.storeDetails.location.latitude;
    this.storeDetails.longitude = this.storeDetails.location.longitude;
    this.slimLoadingBarService.start();
    this.storeService.updateStore(this.storeDetails).subscribe((data) => {
      this.isSuccess = true;
      this.showMsg = true;
      this.slimLoadingBarService.complete();
      setTimeout(() => {
        this.isVisible = false;
        this.showMsg = false;
        this.showStoreHome.emit({ rid: this.storeDetails.retailerId });
      }, 2000)
    }, () => {
      this.isSuccess = false;
      this.showMsg = true;
    });
  }

  public OnCancel() {
    this.isVisible = false;
    this.showStoreHome.emit({ rid: this.storeDetails.retailerId });
    this.clear();
  }

  public clear() {
    this.storeDetails = new StoreDetails();
  }
}
