import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdService } from '../service/ad.service';
import { AdDetails } from '../model/adDetails';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-ad-details',
  templateUrl: './ad-details.component.html',
  styleUrls: ['./ad-details.component.scss']
})
export class AdDetailsComponent implements OnInit {
  // private images: { title, path, validFor, Offer }[] = [];
  id: number;
  isVisible: boolean = false;
  private adDetails: AdDetails = new AdDetails();
  originalImages: { url: string, pageNumber: number }[] = [];
  optimized1Images: { url: string, pageNumber: number }[] = [];
  optimized2Images: { url: string, pageNumber: number }[] = [];
  address: string;
  totalImageCount: number = 0;
  @Output() public showHome = new EventEmitter<any>();
  screenHeight: number;
  screenWidth: number;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
  }
  isSmallScreen: boolean = false;

  constructor(private adService: AdService, private route: ActivatedRoute, private router: Router) {
    this.onResize();
    if (this.screenWidth <= environment.smallScreenWidth) {
      this.isSmallScreen = true;
    }
    else {
      this.isSmallScreen = false;
    }
    // this.route.params.subscribe(params => {
    //   this.id = +params['id']; // (+) converts string 'id' to a number
    // });
    // this.adService.getAdDetailsById(this.id).subscribe((data) => {
    //   this.adDetails = new AdDetails(data);
    //   this.originalImages = this.adDetails.allImageUrlsOrignials ? this.adDetails.allImageUrlsOrignials : [];
    //   this.optimized1Images = this.adDetails.allImageUrlsOptimized1 ? this.adDetails.allImageUrlsOptimized1 : [];
    //   this.optimized2Images = this.adDetails.allImageUrlsOptimized2 ? this.adDetails.allImageUrlsOptimized2 : [];
    //   this.totalImageCount = this.originalImages ? this.originalImages.length : 0;
    //   this.prepareAddress();
    // });
  }

  ngOnInit() {
  }

  open(adId: number) {
    this.isVisible = true;
    this.id = adId;
    this.originalImages = [];
    this.adService.getAdDetailsById(this.id).subscribe((data) => {
      this.adDetails = new AdDetails(data);
      if (this.isSmallScreen) {
        this.originalImages = this.adDetails.allImageUrlsOptimized1 ? this.adDetails.allImageUrlsOptimized1 : [];
      } else {
       this.originalImages = this.adDetails.allImageUrlsOrignials ? this.adDetails.allImageUrlsOrignials : [];
      this.optimized1Images = this.adDetails.allImageUrlsOptimized1 ? this.adDetails.allImageUrlsOptimized1 : [];
      this.optimized2Images = this.adDetails.allImageUrlsOptimized2 ? this.adDetails.allImageUrlsOptimized2 : [];
      }
      this.totalImageCount = this.originalImages ? this.originalImages.length : 0;
      this.prepareAddress();
    });
  }

  close() {
    this.isVisible = false;
    this.showHome.emit({});
  }

  prepareAddress() {
    let storeName: string;
    let address: string;
    let landmark: string;
    let phone: string;
    if (this.adDetails.storeDetails) {
      storeName = this.adDetails.storeDetails.storeName ? this.adDetails.storeDetails.storeName + ", " : "";
      address = this.adDetails.storeDetails.address ? this.adDetails.storeDetails.address + ", ": "";
      landmark = this.adDetails.storeDetails.landmark ? this.adDetails.storeDetails.landmark  : "";
      phone = this.adDetails.storeDetails.phone ? this.adDetails.storeDetails.phone.toString() : "N/A";

      if(phone !== "N/A"){
      this.address = storeName + address + landmark + ", Phone: " + phone;
      }else{
        this.address = storeName + address + landmark;
      }
    }
  }
}
