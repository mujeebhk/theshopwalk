export class UrlHelper {
    public static buildUrl(url: string, parameters: { [key: string]: string; } = undefined) {
        // TODO
        let apiEndPoint: any = "https://theshopwalk-211808.appspot.com/shopWalk"
        // let apiEndPoint: any = "https://9080-dot-4329690-dot-devshell.appspot.com/shopWalk"
        if (url !== undefined && url !== null && url.length > 0) {
            if (url[0] === "/") {
                url = apiEndPoint + url;
            } else {
                url = apiEndPoint + "/" + url;
            }
        }

        if (parameters !== undefined && parameters !== null) {
            let parameterString;

            for (let key in parameters) {
                let value = parameters[key];
                if (value !== undefined && value !== null) {
                    if (parameterString !== undefined && parameterString !== null) {
                        parameterString += "&" + key + "=" + value;
                    } else {
                        parameterString = "?" + key + "=" + value;
                    }
                }
            }

            if (parameterString !== undefined && parameterString !== null) {
                url += parameterString;
            }
        }

        return url;
    }
}