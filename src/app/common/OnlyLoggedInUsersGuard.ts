import { Injectable } from "../../../node_modules/@angular/core";
import { CanActivate, Router } from "../../../node_modules/@angular/router";
import { AdminService } from "../service/admin.service";
import { Observable } from "../../../node_modules/rxjs";
import { map, take } from "../../../node_modules/rxjs/operators";

@Injectable()
export class OnlyLoggedInUsersGuard implements CanActivate {
    constructor(private router: Router, private adminService: AdminService) { };

    canActivate(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.adminService.isLoggedIn().then((data) => {
                let result: boolean;
                if (data.isLoggedIn) {
                    result = true;
                } else {
                    window.alert("You don't have permission to view this page");
                    this.router.navigate(['/tswlogin']);
                    result = false;
                }
                resolve(result);
            });
        });
    }
}