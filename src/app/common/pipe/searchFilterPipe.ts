import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { DynamicHelper } from '../helpers/dynamic.helper';

@Pipe({
    name: 'searchFilter'
})
@Injectable()
export class SearchFilterPipe implements PipeTransform {
    transform(items: any[], args: string, seachColumn: string, sortColumn: string, reverseOrder: boolean = false): any {
        let result: any[];
        if (!args) {
            result = items ? items.sort(DynamicHelper.dynamicSort(sortColumn)) : items;
        }
        else if (items) {
            result = items.filter(item => item[seachColumn].toLowerCase().includes(args.toLowerCase())).sort(DynamicHelper.dynamicSort(sortColumn));
        }
        else {
            result = items ? items.sort(DynamicHelper.dynamicSort(sortColumn)) : items;
        }

        if (reverseOrder) {
            return result ? result.reverse() : result;
        }
        else {
            return result;
        }
    }
} 