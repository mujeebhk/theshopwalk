import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AdDetails } from '../model/adDetails';
import { AdService } from '../service/ad.service';
import { SlimLoadingBarService } from '../../../node_modules/ng2-slim-loading-bar';

@Component({
  selector: 'app-ad-view-images',
  templateUrl: './ad-view-images.component.html',
  styleUrls: ['./ad-view-images.component.scss']
})
export class AdViewImagesComponent implements OnInit {
  isVisible: boolean;
  adId: number;
  adDetails: AdDetails;
  images: { url: string, pageNumber: number }[] = [];

  @Output() public showAdHome = new EventEmitter<any>();

  constructor(private adService: AdService, private slimLoadingBarService: SlimLoadingBarService) { }

  ngOnInit() {
  }

  open(aId: number) {
    this.slimLoadingBarService.start();
    this.isVisible = true;
    this.adId = aId;
    this.adService.getAdDetailsById(this.adId).subscribe((data) => {
      this.adDetails = new AdDetails(data);// using constructor will populate other calculated properties
      console.log(this.adDetails);
      if (this.adDetails.allImageUrlsOrignials) {
        this.images = this.adDetails.allImageUrlsOrignials;
      }
      console.log(this.images);
    }, () => {
      this.slimLoadingBarService.complete();
    }, () => {
      this.slimLoadingBarService.complete();
    });
  }

  public OnClose() {
    this.clear();
    this.isVisible = false;
    this.showAdHome.emit({ rid: this.adDetails.retailerId });
  }

  public clear() {
    this.adDetails = new AdDetails();
  }
}
