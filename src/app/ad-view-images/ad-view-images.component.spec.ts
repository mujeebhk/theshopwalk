import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdViewImagesComponent } from './ad-view-images.component';

describe('AdViewImagesComponent', () => {
  let component: AdViewImagesComponent;
  let fixture: ComponentFixture<AdViewImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdViewImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdViewImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
