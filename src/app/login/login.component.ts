import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../service/admin.service';
import { AdminDetails } from '../model/adminDetails';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private userName: string;
  private password: string;
  private isInvalid: boolean = false;
  constructor(private router: Router, private adminService: AdminService) {
    this.userName = "";
    this.password = "";
  }
  ngOnInit() {
  }

  private onLoginClick() {
    let adminDetails = new AdminDetails();
    adminDetails.username = this.userName;
    adminDetails.password = this.password;
    this.adminService.login(adminDetails).subscribe(() => {
      this.router.navigate(['/dashboard']);
      this.isInvalid = false;
    }, () => {
      this.isInvalid = true;
    });
  }
}
