export const environment = {
  production: true,
  googleLocationCountry: "AE",
  defaultLatitude: 13.0215614,
  defaultLongitude: 77.6056505,
  smallScreenWidth: 767,
  MediumScreenWidthMin: 768,
  MediumScreenWidthMax: 1269,
  LargeScreenWidth: 1270
};
